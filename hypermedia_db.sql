-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 21, 2016 at 09:35 AM
-- Server version: 10.1.17-MariaDB
-- PHP Version: 7.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hypermedia_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ABOUTUS`
--

CREATE TABLE `ABOUTUS` (
  `id` int(11) NOT NULL,
  `section` enum('park','activities') NOT NULL,
  `title` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ABOUTUS`
--

INSERT INTO `ABOUTUS` (`id`, `section`, `title`, `description`, `img`) VALUES
(0, 'activities', 'Vivere la natura in modo più lento, più dolce, più profondo', 'Scopri il Parco Naturale Adamello Brenta in tutte le stagioni.\r\n<br>\r\nIntraprendi un viaggio alla ricerca della varietà della natura, dei suoi colori e dei segni che l’uomo vi ha impresso attraverso le attività tradizionali legate alla montagna. Lasciati guidare, per qualche ora, una giornata o un week end, alla scoperta di contatti dimenticati con il paesaggio, fatti di immagini, suoni e percezioni sensoriali. Assapora il gusto delle passeggiate, del respirare a pieni polmoni, dell’arricchirsi di conoscenze preziose sulla flora e sulla fauna del Parco Naturale Adamello Brenta. Cammina lentamente lungo la rete di oltre 700 chilometri di sentieri segnalati all’interno dell’area protetta. Cogli l’opportunità per osservare i segni e le tracce che gli animali selvatici lasciano sul terreno. Fermati ad ammirare boschi e praterie, vette e ghiacciai, il tramonto e il cielo stellato. ', ''),
(1, 'activities', 'Attività estive', 'Avventura o relax, svago o riposo, qualunque sia la tua idea di vacanza, è la natura che sa regalare a tutti i gusti la Malga Campo e Val di Nardisperfetta ambientazione per un’esperienza appagante tra stupore e rispetto.\r\n<br>\r\nGrazie alla preziosa collaborazione che, anno dopo anno, si è instaurata con le ApT, le Pro Loco e i Consorzi turistici del territorio, il Parco si è confermato uno dei protagonisti dell’estate, di chi vuole continuare a scoprire e apprendere sfruttando tutte le opportunità del territorio.\r\n<br>\r\nSuggestive escursioni con l’accompagnamento degli educatori ambientali, dei guardaparco e delle guide alpine conducono nei luoghi più belli e suggestivi dell’area protetta. Facili camminate alla scoperta dei segreti dell\'area protetta, oppure passeggiate al tramonto e sotto le stelle sono alcune delle esperienze che si possono vivere partecipando ad una delle attività proposte dal programma “Un’estate da Parco”.\r\n<br>\r\nAncora, si può scegliere tra un’uscita di mezza giornata alla scoperta dei luoghi più selvaggi del Parco Naturale Adamello Brenta, i preferiti dall’orso bruno, ed escursioni per conoscere i segni, ancora impressi sul territorio, del ghiacciaio che c’era in Val Genova. Alla Val di Tovel e alla Val Genova sono inoltre dedicate attività giornaliere che spaziano dal teatro scientifico alle camminate per conoscere le specificità dei luoghi. Impegnative, ma affascinanti, sono le traversate delle Dolomiti di Brenta e della Presanella, mentre le escursioni sugli alpeggi della Rendena si rivolgono a tutti i visitatori. ', 'attività-estive.jpg'),
(2, 'activities', 'Attività invernali', 'Infiniti cristalli di neve scendono su boschi e foreste, avvolgendo in un dolce sonno la natura che segue il lento ritmo dell’inverno. Il paesaggio del Parco Naturale Adamello Brenta è disegnato dalla magia di un bianco mantello e dai giochi di luce che il sole crea illuminando l’ambiente invernale. Ma la natura dorme solo apparentemente e gli animali di cui il Parco è straordinariamente ricco – dalle aquile reali ai galli cedroni, dalle marmotte agli stambecchi fino all’orso bruno – svelano la loro presenza lasciando chiare tracce sul manto nevoso.\r\n<br>\r\nQuesta è la natura incantata del Parco d’inverno, che ti invitiamo a scoprire lontano dalla confusione delle piste da sci, alla ricerca di un rapporto intimo con la natura. ', 'attività-invernali.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `HH`
--

CREATE TABLE `HH` (
  `id` varchar(191) NOT NULL,
  `name` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `services` text NOT NULL,
  `address` varchar(191) NOT NULL,
  `phone` varchar(191) NOT NULL,
  `fax` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `site` varchar(191) NOT NULL,
  `rate` enum('1','2','3','4','5') NOT NULL,
  `img` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `HH`
--

INSERT INTO `HH` (`id`, `name`, `description`, `services`, `address`, `phone`, `fax`, `email`, `site`, `rate`, `img`) VALUES
('alphotel_milano', 'Alphotel Milano', 'Cercate un hotel 3 stelle ad Andalo, per una vacanza rigenerante? Fermatevi qui, l’avete trovato.\r\nAlphotel Milano vi offre tutte le comodità di soggiornare in posizione centrale, nella parte pianeggiante del paese, senza rinunciare al magnifico panorama delle Dolomiti di Brenta. Alphotel Milano non è solo un family hotel ad Andalo, ma è il posto ideale per una vacanza benessere o per un periodo di relax tra natura e gastronomia.\r\n<br><br>\r\nLa vacanza sportiva ad Andalo è all’Alphotel Milano. Siamo a soli 400 metri dalla partenza degli impianti della Ski Area Paganella, per ospitarvi durante la vostra settimana bianca, e nel cuore delle Dolomiti di Brenta, scenario ideale per la mountain bike in Trentino. E se cercate altri sport, a 500 metri c’è Andalo Life Park, 130.000 mq di attività per tutte le stagioni.<br><br>', '<strong>Sala per bambini</strong><br>\nUna stanza tutta per loro: è quella che mettiamo a disposizione dei bambini, per giocare e divertirsi. E d’estate, lo spazio giochi è anche in giardino!<br><br>\n<strong>Wi-Fi</strong><br>\nTutti gli spazi dell’albergo sono coperti da connessione wi-fi gratuita.<br><br>\n<strong>Giardino</strong><br>\nPer prendere il sole, far giocare i bambini, o per godersi un momento di relax, un angolo verde tutto per voi.<br><br>\n<strong>Parcheggio privato</strong><br>\nIn vacanza parcheggiare non deve essere un problema: ampio parcheggio gratuito per i nostri ospiti.<br><br>\n<strong>Centro benessere</strong><br>\nCon idromassaggio, sauna, bagno turco e doccia emozionale. E naturalmente, vista sulle Dolomiti di Brenta.<br><br>\n<strong>Andalo Card</strong><br>\nGli sconti per la vostra vacanza ve li dà Andalo Card, per i nostri ospiti disponibile gratuitamente in albergo.<br><br>\n<strong>Deposito sci e bici</strong><br>\nIl posto per la vostra attrezzatura sportiva: in inverno asciuga-scarponi riscaldante, in estate tutto lo spazio per le vostre biciclette.<br><br>\n<strong>Skibus gratuito</strong><br>\nFerma nel parcheggio dell’hotel, per raggiungere le piste durante la vostra settimana bianca in Trentino. <br><br>', 'Via Dossi, 8\r\n<br>\r\n38010 Andalo', '0461/585731', '0461/585378', 'info@alphotelmilano.it', 'www.alphotelmilano.it', '3', 'hotel-andalo-cuore-dolomiti.jpg'),
('holiday_dolomiti', 'Alp Holiday Dolomiti', 'Nel nostro hotel 4 stelle in Val di Sole ci prendiamo cura del vostro benessere, e vi proponiamo esperienze e offerte per scoprire il Trentino, lasciateci esaudire i vostri desideri!<br>\r\nRiscoprite momenti romantici di coppia mentre noi ci prendiamo cura dei vostri bambini per 12 ore al giorno con giochi e attività creative.<br>\r\nSoggiorna da noi e potrai ricevere la \r\nVAL DI SOLE OPPORTUNITY-GUEST CARD TRENTINO', '<b>Camere</b>: arredate su misura in abete massello dispongono di pavimento in moquette o legno naturale. Il bagno è fornito di linea cortesia, asciugacapelli, specchio per trucco, accappatoio per accedere direttamente al centro benessere. La camera ed il bagno sono dotati di riscaldamento automatizzato a pavimento e termosifone.\r\n<br>\r\nTutte le stanze dispongono di minibar, cassaforte, allacciamento internet, sky gold tv sat e balcone.\r\n<br><br>\r\n<b>Parco Card</b>: “il parco a portata di mano” numerose attività per famiglie, sportive, enogastronomiche e culturali gratis per voi.\r\n<br><br>\r\n<b>Connessione internet</b>: collegamento senza fili “wireless” gratuito in tutto l’hotel e camere. A disposizione un PC portatile.\r\n<br><br>\r\n<b>Bar, soggiorno</b>: aperti dalle 07.30 alle 24.00. Nel pomeriggio potrete usufruire di uno spuntino per reintegrare l’energia consumata durante la giornata.\r\n<br><br>\r\n<b>Giardino d\'inverno</b>: la visione nell’insieme riecheggia il “british style” per il colore, un classico verde inglese del giardino esterno e per la struttura dell’ambiente; qui potrete rilassarvi magari sorseggiando un buon vino Trentino, Nazionale o una tisana Biologica.\r\n<br><br>\r\n<b>Wine bar smoking room</b>: una sala per fumatori con servizio bar arredata in legno d’abete Trentino lavorato a mano.\r\n<br><br>\r\n<b>Sala giochi per bambini</b>: adatta per i più piccoli ma anche con giochi per i più grandi.\r\n<br><br>\r\n<b>Servizio baby sitting</b>: su richiesta un servizio adatto alle esigenze dei vostri bambini.\r\n<br><br>\r\n<b>Garage su richiesta</b>: gratuito per chi soggiorna nelle suite.\r\n<br><br>\r\n<b>Deposito sci</b>: riscaldato dove potrete custodire sci, scarponi, snowboard e boots nel vostro armadietto privato dotato di scalda scarponi.\r\n<br><br>\r\n<b>Deposito per biciclette</b>: al coperto e custodito con kit riparazioni.\r\n<br><br>\r\n<b>Giardino</b>: con parco giochi, mountain bike e city bike a disposizione, terrazze solarium.\r\n<br><br>\r\n<b>Accesso disabili</b>\r\n<br><br>\r\n<b>Servizi utili</b>:\r\n<br>\r\nServizio lavanderia per lavaggio e stiratura\r\n<br>\r\nServizio fax\r\n<br>\r\nServizio sveglia\r\n<br>\r\nServizio in camera\r\n<br>\r\nSi accettano tutte le carte di credito\r\n<br>\r\nSki shuttle privato (usufruibile durante l’inverno)\r\n<br>\r\nBox lunch\r\n<br>\r\nServizio di pensione completa\r\n<br>\r\nMaestro di sci/snowboard in hotel\r\n<br><br>\r\nTransfer dagli aeroporti e stazioni ferroviare direttemente all’hotel ', 'Via Campiglio, 4 <br> 38025 Dimaro', '0463/973330', '0463/974287', 'info@alpholiday.it', 'www.alpholiday.it', '4', 'alpholiday-dolomiti-wellness.jpg'),
('hotel_denny', 'Hotel Denny', 'Hotel Denny and Apartaments*** Pinzolo Carisolo (Trentino)<br>\r\nMadonna di Campiglio   <br>\r\na 500m dalle Funivie Pinzolo, all Imbocco della Val Genova nel Parco Naturale Adamello Brenta, a 3 km dalle Cascate Nardis, e a 10 km da Madonna di Campiglio. L\'hotel Denny è in posizione ideale per le Vostre Vacanze, in Estate oppure in Inverno! Ottimi prezzi e qualità! \r\n<br><br>\r\nVuoi una vacanza diversa? Nella Natura ma con tanti servizi? Ti piace il Trekking, passeggiare, escursioni guidate?\r\nApprofitta dei nostri pacchetti Vacanza Attiva Parco Card oppure Dolomeet Card: mini pacchetti da 3 giorni oppure 7 giorni ! !\r\n<br><br>\r\nInoltre troverete sempre prodotti freschi provenienti direttamente dalla nostra Azienda Agricola: uova, frutti di bosco, marmellate fatte in casa, frutta ed ortaggi....!\r\n<br><br>\r\nNovità! Siamo entrati a far parte di QUALITA\' PARCO ADAMELLO !!! Un marchio di Qualità rilasciato solamente alle strutture che seguono la politica del Parco Adamello Brenta, che credono nello sfruttamento intelligente delle risorse energetiche ed utilizzano prodotti del Territorio!\r\n<br><br>\r\nVuoi lo skipass sul tuo comodino all\'arrivo dell\'hotel? BASTA LUNGHE CODE PER ACQUISTARE LO SKIPASS! Approfitta dei nostri pacchetti comprensivi di skipass e potrai ritirare lo skipass direttamente alla reception!\r\n<br><br>\r\nVIVI L\'INVERNO CON NOI! Miniclub GRATUITO per i nostri piccoli clienti sulle piste da sci, Ciaspolate GRATUITE, Passeggiate con le Guide Alpine GRATUITE! Queste sono le attività a disposizione dei nostri clienti per questo INVERNO, per godere della natura e quel qualcosa in più che Pinzolo sa dare! Clicca Qui! \r\n<br><br>\r\nVisita il nostro Hotel oppure Appartamenti in Hotel oppure Dependance, scegli la tua vacanza: la comodità dell\'Hotel o la libertà degli appartamenti tutto compreso!\r\nHotel dotato anche di Appartamenti. Gestione familiare, posizionato in zona tranquilla ma a 10 minuti in passeggiata dal centro di Pinzolo nella splendida cornice della Val Rendena, all\'imbocco della Val Genova e a pochi km da Madonna di Campiglio. Al centro di Carisolo, a 500 m dagli impianti di risalita di Pinzolo servito da bus navetta privato e gratuito. Le camere sono anallergiche dotate di tutti i confort tra i quali parquet, bagno privato con doccia, TV a schermo piatto con canali via satellite,balcone, asciuigacapelli, cassaforte rete wi-fi in tutte le camere. Cucina particolarmente curata con prodotti del luogo e ingredienti genuini, pasta fatta in casa, gelato artigianale e dolci di nostra produzione con utilizzo dei prodotti coltivati naturalmente nella nostra azienda agricola. Particolarmente attenti ai problemi di diete particolari e cucina senza glutine secondo metodologia A.I.C', 'I servizi garantiti ad ogni stagione:<br>\r\n- Ampio Parcheggio Gratuito Privato<br>\r\n- Cucina Casalinga dalla Pasta al Dolce di Propria Produzione,Cucina Tipica Trentina,Menu’ con Ampia Scelta e Assortimento, Buffett di Verdure a Pranzo e Cena, Colazione a Buffett dolce e salato.<br>\r\n- Bar ed ampi spazi riservati ai clienti dell’hotel.<br>\r\n- Convenzionato Terme Val Rendena.<br>\r\n- Convenzionato Golf Club Val Rendena<br>\r\n- Sala giochi con ping pong e calcetto.<br>\r\n- Veranda esterna - Solarium.<br>\r\n- Giardino con Gazebo.<br>\r\n- Idrosauna con Aromaterapia.<br>\r\n- Deposito sci con Scaldascarponi e Scaldagiacche<br>\r\n- In fronte alla fermata Skibuss Madonna di Campiglio<br>\r\n- Navetta gratuita da e per le Funivie Pinzolo<br>\r\n- Noleggio Mountain Bike.<br>\r\n- Palestra attrezzata Fitness.<br>\r\n- TV internazionale in camera con alcuni canali via satellite.<br>\r\n- Sala TV.<br>\r\n- Tavernetta per feste.<br>\r\n- Accesso Internet Wi-Fi GRATUITO in tutto l\'hotel!<br>\r\n- Telefono diretto.<br>\r\n- Tutte le camere con servizi privati<br>\r\n- Tutte le camere con balcone<br>\r\n- Docce di Servizio “last day” così puoi andare a sciare fino a tardo pomeriggio l\'ultimo giorno di permanenza, anche se avrai già liberato la camera, prima di partire potrai utilizzare le nostre docce<br>\r\n- Lavanderia a Gettone<br>\r\n- Ecocompatibilità: utilizzo integrato di fonti energetiche rinnovabili, pannelli solari FOTOVOLTAICI, collettori SOLARI, caldaia a BIOMASSA<br>\r\n- Riscaldamento: utilizzo unicamente di sitemi moderni basso emissivi ed alta resa<br>\r\n- La struttura: Alta coibentazione per maggior confort e basso impatto energetico\r\n\r\n', 'Via Garibaldi, 4 <br> 38080 Carisolo', '0465/501398', '0465/501398', 'info@hoteldenny.it', 'www.hoteldenny.it', '3', 'foto-hotel-denny_239631.jpeg'),
('hotel_gianna', 'Hotel Gianna', 'L’hotel con <b>centro benessere</b> e <b>piscina</b> si trova in una posizione tranquilla, in estate un ampio giardino circondato da abeti fa da cornice alla struttura.\r\n<br><br>\r\nDa qui si diramano numerose <b>passeggiate</b> per tutti i tipi di percorsi e di gambe, le cascate di Vallesinella, i rifugi del Brenta, le malghe per l’alpeggio delle mucche, vi lasceranno incantati.\r\n<br><br>\r\nIn inverno, le <b>piste da sci</b> tutte collegate, vi permetteranno di sfruttare le discese della skiarea Madonna di Campiglio (150 km di piste) circondati dallo spettacolare panorama delle Dolomiti.\r\n<br><br>\r\nL’atmosfera si vive anche nell’albergo, gli ambienti sono stati rinnovati rispettando il gusto e lo stile montano, il legno, la stube, i profumi, il silenzio, la tranquillità, sono ideali per chi vuole stare bene.\r\n<br><br>\r\nLa nostra struttura rispetta, in modo rigoroso, fondamentali regole per sostenere sempre più una coscienza <b>eco-friendly</b>:\r\n<br> \r\n- Riduzione e riciclo (attraverso la raccolta differenziata) dei rifiuti prodotti\r\n<br>\r\n- Risparmio energetico, grazie all’adozione delle moderne tecnologie disponibili\r\n<br>\r\n- Biosfloor caldane pavimento a secco con granulato naturale \r\n<br>\r\n- Promuoviamo il nostro territorio, i beni naturalistici e culturali, con menù che valorizzino l’enogastronomia tipica del luogo e proposte di itinerari ad hoc, a stretto contatto con il paesaggio circostante (DoloMeetCard, ParcoCard, GuestCard)\r\n<br>\r\n- Cerchiamo di coinvolgere i turisti nella sostenibilità e nel rispetto dell\'ambiente, favorendone comportamenti corretti per renderli altrettanto responsabili\r\n<br><br>\r\nAltre azioni attive per il raggiungimento degli obiettivi “green”:\r\n<br>\r\n- Cappotto termico con lo scopo di ottimizzare la prestazione termica dell’edificio eliminando quei punti della struttura in cui si hanno delle vie preferenziali per la dispersione del calore\r\n<br>\r\n- Domotica per la gestione dell\'intera struttura, la supervisione degli impianti, l’ottimizzazione delle risorse, la semplicità d\'uso e il risparmio energetico\r\n<br>\r\n- Uso del pellet in sostituzione dei combustibili fossili per la produzione dell\'acqua calda e del riscaldamento\r\n<br>\r\n- Ottenimento del certificato ambientale Qualità Parco; La verifica del soddisfacimento dei requisiti green viene svolta da un ente internazionale, il Det Norske Veritas', '<b>Camere</b>: arredate su misura in abete massello dispongono di pavimento in moquette o legno naturale. Il bagno è fornito di linea cortesia, asciugacapelli, specchio per trucco, accappatoio per accedere direttamente al centro benessere. La camera ed il bagno sono dotati di riscaldamento automatizzato a pavimento e termosifone.\r\n<br>\r\nTutte le stanze dispongono di minibar, cassaforte, allacciamento internet, sky gold tv sat e balcone.\r\n<br><br>\r\n<b>Parco Card</b>: “il parco a portata di mano” numerose attività per famiglie, sportive, enogastronomiche e culturali gratis per voi.\r\n<br><br>\r\n<b>Connessione internet</b>: collegamento senza fili “wireless” gratuito in tutto l’hotel e camere. A disposizione un PC portatile.\r\n<br><br>\r\n<b>Bar, soggiorno</b>: aperti dalle 07.30 alle 24.00. Nel pomeriggio potrete usufruire di uno spuntino per reintegrare l’energia consumata durante la giornata.\r\n<br><br>\r\n<b>Giardino d\'inverno</b>: la visione nell’insieme riecheggia il “british style” per il colore, un classico verde inglese del giardino esterno e per la struttura dell’ambiente; qui potrete rilassarvi magari sorseggiando un buon vino Trentino, Nazionale o una tisana Biologica.\r\n<br><br>\r\n<b>Wine bar smoking room</b>: una sala per fumatori con servizio bar arredata in legno d’abete Trentino lavorato a mano.\r\n<br><br>\r\n<b>Sala giochi per bambini</b>: adatta per i più piccoli ma anche con giochi per i più grandi.\r\n<br><br>\r\n<b>Servizio baby sitting</b>: su richiesta un servizio adatto alle esigenze dei vostri bambini.\r\n<br><br>\r\n<b>Garage su richiesta</b>: gratuito per chi soggiorna nelle suite.\r\n<br><br>\r\n<b>Deposito sci</b>: riscaldato dove potrete custodire sci, scarponi, snowboard e boots nel vostro armadietto privato dotato di scalda scarponi.\r\n<br><br>\r\n<b>Deposito per biciclette</b>: al coperto e custodito con kit riparazioni.\r\n<br><br>\r\n<b>Giardino</b>: con parco giochi, mountain bike e city bike a disposizione, terrazze solarium.\r\n<br><br>\r\n<b>Accesso disabili</b>\r\n<br><br>\r\n<b>Servizi utili</b>:\r\n<br>\r\nServizio lavanderia per lavaggio e stiratura\r\n<br>\r\nServizio fax\r\n<br>\r\nServizio sveglia\r\n<br>\r\nServizio in camera\r\n<br>\r\nSi accettano tutte le carte di credito\r\n<br>\r\nSki shuttle privato (usufruibile durante l’inverno)\r\n<br>\r\nBox lunch\r\n<br>\r\nServizio di pensione completa\r\n<br>\r\nMaestro di sci/snowboard in hotel\r\n<br><br>\r\nTransfer dagli aeroporti e stazioni ferroviare direttemente all’hotel Gianna', 'Via Vallesinella, 16\r\n<br>\r\n38084 Madonna di Campiglio', '0465/441106', '0465/440775', 'hotelgianna@hotelgianna.it ', 'www.hotelgianna.it', '4', 'hotel-gianna.jpg'),
('hotel_regina_elena', 'Hotel Regina Elena', 'Ci troviamo nel piccolo borgo medievale di Caderzone Terme a 3km da Pinzolo, in mezzo a verdi prati e antichi masi, circondati dalle montagne del Parco Naturale Adamello Brenta, la più vasta area protetta del Trentino.\r\n<br><br>\r\nUn hotel che ha ottenuto i riconoscimenti "Ecoristorazione Trentino", "Qualità Parco" “Carta Europea per il Turismo Sostenibile” e “Trentinerbe” per la sostenibilità, la tutela dell´ambiente e il legame con il territorio.\r\n<br><br>\r\nTra le tante iniziative possiamo ricordare: il riscaldamento con caldaie a pellets dell\'intera struttura, la produzione di energia elettrica con pannelli fotovoltaici, l\'acquisto di ulteriore energia elettrica da fonti rinnovabili al 100%, l\'utilizzo di lampadine a led, l\'attenta gestione dei rifiuti, il sostegno a Slow Food, a Emergency e a un´agricoltura poco impattante come la biologica con l’utilizzo di oltre 30 prodotti bio come base per la nostra cucina…\r\n<br><br>\r\n\r\nUna famiglia pronta a darvi il benvenuto e farvi scoprire il territorio e la cultura trentina, compresi i prelibati sapori della tradizione agricola locale. Offriamo bontà e genuinità in maniera eco-sostenibile e con coscienza etica, coniugando qualità, salute e piaceri della tavola, per offrirvi un\'alimentazione sana e gustosa. \r\n<br><br>\r\n\r\nDisponiamo di 28 camere con servizi privati, asciugacapelli, cassaforte, balcone, tv e telefono, ascensore, bar-sala tv, ristorante, angolo stube e parcheggio privato. L\'ambiente è caldo e raffinato. Suggestioni di paesaggi montani e rurali sono i soggetti dei dipinti realizzati dall´artista di casa, Vlajko, da ammirare in ogni stanza e nelle sale comuni. Gli interni sono studiati per far sentire l\'ospite a casa. Il gradevole arredo di abete bianco nelle camere, il ciliegio nelle sale. Siamo convenzionati con le Terme Val Rendena e il centro benessere a soli 100 mt dall’hotel!', '<b>Camere</b>: arredate su misura in abete massello dispongono di pavimento in moquette o legno naturale. Il bagno è fornito di linea cortesia, asciugacapelli, specchio per trucco, accappatoio per accedere direttamente al centro benessere. La camera ed il bagno sono dotati di riscaldamento automatizzato a pavimento e termosifone.\r\n<br>\r\nTutte le stanze dispongono di minibar, cassaforte, allacciamento internet, sky gold tv sat e balcone.\r\n<br><br>\r\n<b>Parco Card</b>: “il parco a portata di mano” numerose attività per famiglie, sportive, enogastronomiche e culturali gratis per voi.\r\n<br><br>\r\n<b>Connessione internet</b>: collegamento senza fili “wireless” gratuito in tutto l’hotel e camere. A disposizione un PC portatile.\r\n<br><br>\r\n<b>Bar, soggiorno</b>: aperti dalle 07.30 alle 24.00. Nel pomeriggio potrete usufruire di uno spuntino per reintegrare l’energia consumata durante la giornata.\r\n<br><br>\r\n<b>Giardino d\'inverno</b>: la visione nell’insieme riecheggia il “british style” per il colore, un classico verde inglese del giardino esterno e per la struttura dell’ambiente; qui potrete rilassarvi magari sorseggiando un buon vino Trentino, Nazionale o una tisana Biologica.\r\n<br><br>\r\n<b>Wine bar smoking room</b>: una sala per fumatori con servizio bar arredata in legno d’abete Trentino lavorato a mano.\r\n<br><br>\r\n<b>Sala giochi per bambini</b>: adatta per i più piccoli ma anche con giochi per i più grandi.\r\n<br><br>\r\n<b>Servizio baby sitting</b>: su richiesta un servizio adatto alle esigenze dei vostri bambini.\r\n<br><br>\r\n<b>Garage su richiesta</b>: gratuito per chi soggiorna nelle suite.\r\n<br><br>\r\n<b>Deposito sci</b>: riscaldato dove potrete custodire sci, scarponi, snowboard e boots nel vostro armadietto privato dotato di scalda scarponi.\r\n<br><br>\r\n<b>Deposito per biciclette</b>: al coperto e custodito con kit riparazioni.\r\n<br><br>\r\n<b>Giardino</b>: con parco giochi, mountain bike e city bike a disposizione, terrazze solarium.\r\n<br><br>\r\n<b>Accesso disabili</b>\r\n<br><br>\r\n<b>Servizi utili</b>:\r\n<br>\r\nServizio lavanderia per lavaggio e stiratura\r\n<br>\r\nServizio fax\r\n<br>\r\nServizio sveglia\r\n<br>\r\nServizio in camera\r\n<br>\r\nSi accettano tutte le carte di credito\r\n<br>\r\nSki shuttle privato (usufruibile durante l’inverno)\r\n<br>\r\nBox lunch\r\n<br>\r\nServizio di pensione completa\r\n<br>\r\nMaestro di sci/snowboard in hotel\r\n<br><br>\r\nTransfer dagli aeroporti e stazioni ferroviare direttemente all’hotel Gianna', 'Via Regina Elena, 35\r\n<br>\r\n38080 CADERZONE', '0465/804722', '0465/804723', 'hotel@reginaelena.com', 'www.reginaelena.com', '3', 'hotel-regina-elena.jpeg'),
('hotel_rio', 'Hotel Rio ***', 'Hotel è situato in un vasto e incantevole parco privato, dove scorre un limpido ruscello con pesci, in una oasi di quiete e verde, a 200 metri dal centro di Caderzone Terme dove si trovano le omonime terme.\r\n<br><br>\r\nPinzolo capoluogo turistico della valle è raggiungibile in soli due minuti con la macchina o con una splendida passeggiata nella grande area naturale che da a Caderzone Terme il titolo di "Cuore verde della Val Rendena".\r\n<br><br>\r\nA servizio dei clienti due ascensori, terrazza solarium, sala lettura, sala TV, taverna, ampio spazio ad uso comune, deposito sci e biciclette, parcheggio privato, internet point , il tutto per garantire il massimo confort.', '<b>Camere</b>: arredate su misura in abete massello dispongono di pavimento in moquette o legno naturale. Il bagno è fornito di linea cortesia, asciugacapelli, specchio per trucco, accappatoio per accedere direttamente al centro benessere. La camera ed il bagno sono dotati di riscaldamento automatizzato a pavimento e termosifone.\r\n<br>\r\nTutte le stanze dispongono di minibar, cassaforte, allacciamento internet, sky gold tv sat e balcone.\r\n<br><br>\r\n<b>Parco Card</b>: “il parco a portata di mano” numerose attività per famiglie, sportive, enogastronomiche e culturali gratis per voi.\r\n<br><br>\r\n<b>Connessione internet</b>: collegamento senza fili “wireless” gratuito in tutto l’hotel e camere. A disposizione un PC portatile.\r\n<br><br>\r\n<b>Bar, soggiorno</b>: aperti dalle 07.30 alle 24.00. Nel pomeriggio potrete usufruire di uno spuntino per reintegrare l’energia consumata durante la giornata.\r\n<br><br>\r\n<b>Giardino d\'inverno</b>: la visione nell’insieme riecheggia il “british style” per il colore, un classico verde inglese del giardino esterno e per la struttura dell’ambiente; qui potrete rilassarvi magari sorseggiando un buon vino Trentino, Nazionale o una tisana Biologica.\r\n<br><br>\r\n<b>Wine bar smoking room</b>: una sala per fumatori con servizio bar arredata in legno d’abete Trentino lavorato a mano.\r\n<br><br>\r\n<b>Sala giochi per bambini</b>: adatta per i più piccoli ma anche con giochi per i più grandi.\r\n<br><br>\r\n<b>Servizio baby sitting</b>: su richiesta un servizio adatto alle esigenze dei vostri bambini.\r\n<br><br>\r\n<b>Garage su richiesta</b>: gratuito per chi soggiorna nelle suite.\r\n<br><br>\r\n<b>Deposito sci</b>: riscaldato dove potrete custodire sci, scarponi, snowboard e boots nel vostro armadietto privato dotato di scalda scarponi.\r\n<br><br>\r\n<b>Deposito per biciclette</b>: al coperto e custodito con kit riparazioni.\r\n<br><br>\r\n<b>Giardino</b>: con parco giochi, mountain bike e city bike a disposizione, terrazze solarium.\r\n<br><br>\r\n<b>Accesso disabili</b>\r\n<br><br>\r\n<b>Servizi utili</b>:\r\n<br>\r\nServizio lavanderia per lavaggio e stiratura\r\n<br>\r\nServizio fax\r\n<br>\r\nServizio sveglia\r\n<br>\r\nServizio in camera\r\n<br>\r\nSi accettano tutte le carte di credito\r\n<br>\r\nSki shuttle privato (usufruibile durante l’inverno)\r\n<br>\r\nBox lunch\r\n<br>\r\nServizio di pensione completa\r\n<br>\r\nMaestro di sci/snowboard in hotel\r\n<br><br>\r\nTransfer dagli aeroporti e stazioni ferroviare direttemente all’hotel Rio', 'Via A. Diaz, 34 <br> 38080 Caderzone', '0465/804288 ', '0465/806249', 'info@hotel-rio.it', 'www.hotel-rio.it', '3', 'hotel-rio.jpg'),
('hotel_tevini', 'Hotel Tevini', 'L’hotel con <b>centro benessere</b> e <b>piscina</b> si trova in una posizione tranquilla, in estate un ampio giardino circondato da abeti fa da cornice alla struttura.\r\n<br><br>\r\nDa qui si diramano numerose <b>passeggiate</b> per tutti i tipi di percorsi e di gambe, le cascate di Vallesinella, i rifugi del Brenta, le malghe per l’alpeggio delle mucche, vi lasceranno incantati.\r\n<br><br>\r\nIn inverno, le <b>piste da sci</b> tutte collegate, vi permetteranno di sfruttare le discese della skiarea Madonna di Campiglio (150 km di piste) circondati dallo spettacolare panorama delle Dolomiti.\r\n<br><br>\r\nL’atmosfera si vive anche nell’albergo, gli ambienti sono stati rinnovati rispettando il gusto e lo stile montano, il legno, la stube, i profumi, il silenzio, la tranquillità, sono ideali per chi vuole stare bene.\r\n<br><br>\r\nLa nostra struttura rispetta, in modo rigoroso, fondamentali regole per sostenere sempre più una coscienza <b>eco-friendly</b>:\r\n<br> \r\n- Riduzione e riciclo (attraverso la raccolta differenziata) dei rifiuti prodotti\r\n<br>\r\n- Risparmio energetico, grazie all’adozione delle moderne tecnologie disponibili\r\n<br>\r\n- Biosfloor caldane pavimento a secco con granulato naturale \r\n<br>\r\n- Promuoviamo il nostro territorio, i beni naturalistici e culturali, con menù che valorizzino l’enogastronomia tipica del luogo e proposte di itinerari ad hoc, a stretto contatto con il paesaggio circostante (DoloMeetCard, ParcoCard, GuestCard)\r\n<br>\r\n- Cerchiamo di coinvolgere i turisti nella sostenibilità e nel rispetto dell\'ambiente, favorendone comportamenti corretti per renderli altrettanto responsabili\r\n<br><br>\r\nAltre azioni attive per il raggiungimento degli obiettivi “green”:\r\n<br>\r\n- Cappotto termico con lo scopo di ottimizzare la prestazione termica dell’edificio eliminando quei punti della struttura in cui si hanno delle vie preferenziali per la dispersione del calore\r\n<br>\r\n- Domotica per la gestione dell\'intera struttura, la supervisione degli impianti, l’ottimizzazione delle risorse, la semplicità d\'uso e il risparmio energetico\r\n<br>\r\n- Uso del pellet in sostituzione dei combustibili fossili per la produzione dell\'acqua calda e del riscaldamento\r\n<br>\r\n- Ottenimento del certificato ambientale Qualità Parco; La verifica del soddisfacimento dei requisiti green viene svolta da un ente internazionale, il Det Norske Veritas', '<b>Camere</b>: arredate su misura in abete massello dispongono di pavimento in moquette o legno naturale. Il bagno è fornito di linea cortesia, asciugacapelli, specchio per trucco, accappatoio per accedere direttamente al centro benessere. La camera ed il bagno sono dotati di riscaldamento automatizzato a pavimento e termosifone.\r\n<br>\r\nTutte le stanze dispongono di minibar, cassaforte, allacciamento internet, sky gold tv sat e balcone.\r\n<br><br>\r\n<b>Parco Card</b>: “il parco a portata di mano” numerose attività per famiglie, sportive, enogastronomiche e culturali gratis per voi.\r\n<br><br>\r\n<b>Connessione internet</b>: collegamento senza fili “wireless” gratuito in tutto l’hotel e camere. A disposizione un PC portatile.\r\n<br><br>\r\n<b>Bar, soggiorno</b>: aperti dalle 07.30 alle 24.00. Nel pomeriggio potrete usufruire di uno spuntino per reintegrare l’energia consumata durante la giornata.\r\n<br><br>\r\n<b>Giardino d\'inverno</b>: la visione nell’insieme riecheggia il “british style” per il colore, un classico verde inglese del giardino esterno e per la struttura dell’ambiente; qui potrete rilassarvi magari sorseggiando un buon vino Trentino, Nazionale o una tisana Biologica.\r\n<br><br>\r\n<b>Wine bar smoking room</b>: una sala per fumatori con servizio bar arredata in legno d’abete Trentino lavorato a mano.\r\n<br><br>\r\n<b>Sala giochi per bambini</b>: adatta per i più piccoli ma anche con giochi per i più grandi.\r\n<br><br>\r\n<b>Servizio baby sitting</b>: su richiesta un servizio adatto alle esigenze dei vostri bambini.\r\n<br><br>\r\n<b>Garage su richiesta</b>: gratuito per chi soggiorna nelle suite.\r\n<br><br>\r\n<b>Deposito sci</b>: riscaldato dove potrete custodire sci, scarponi, snowboard e boots nel vostro armadietto privato dotato di scalda scarponi.\r\n<br><br>\r\n<b>Deposito per biciclette</b>: al coperto e custodito con kit riparazioni.\r\n<br><br>\r\n<b>Giardino</b>: con parco giochi, mountain bike e city bike a disposizione, terrazze solarium.\r\n<br><br>\r\n<b>Accesso disabili</b>\r\n<br><br>\r\n<b>Servizi utili</b>:\r\n<br>\r\nServizio lavanderia per lavaggio e stiratura\r\n<br>\r\nServizio fax\r\n<br>\r\nServizio sveglia\r\n<br>\r\nServizio in camera\r\n<br>\r\nSi accettano tutte le carte di credito\r\n<br>\r\nSki shuttle privato (usufruibile durante l’inverno)\r\n<br>\r\nBox lunch\r\n<br>\r\nServizio di pensione completa\r\n<br>\r\nMaestro di sci/snowboard in hotel\r\n<br><br>\r\nTransfer dagli aeroporti e stazioni ferroviare direttemente all’hotel Gianna', 'Frazione Almazzago\r\n<br>\r\n38020 Commezzadura', '0463/974985', '0465/804723', 'info@hoteltevini.com', 'www.hoteltevini.com', '4', 'hotel-tevini.jpg'),
('hotel_vittoria', 'Hotel Vittoria', 'L\'Hotel Vittoria di Dimaro in Val di Sole ti offre un ambiente accogliente e cordiale tra gli emozionanti scenari delle Dolomiti di Brenta e del Parco Naturale Adamello Brenta. L\'atmosfera ideale per una vacanza sportiva con amici o speciali momenti di relax con la famiglia in Trentino.', 'TANTE COMODITÀ PER LA VACANZA IN VAL DI SOLE IN TRENTINO\r\n<br><br>\r\nTrattamento “Mezza pensione Vittoria” con <br>colazione a buffet, cena con menu gourmet e light lunch dalle 12.30 alle 14.30 con buffet di insalate, piatti freddi e stuzzichini<br>\r\nMenù e prodotti gluten-free (su richiesta)<br>\r\nWiFi gratuito in tutto l’Hotel e nelle stanze<br>\r\nDeposito bagagli<br>\r\nParcheggio privato gratuito all\'aperto o garage custodito (a pagamento)<br>\r\nCentro Benessere con sauna, bagno turco, doccia multifunzione, palestra attrezzata Technogym, poltrona massaggi relax, vasca idromassaggio cuore (a pagamento)<br>\r\nAnimali domestici benvenuti (con supplemento)\r\n <br><br>\r\nPER LE FAMIGLIE CON BAMBINI\r\n<br><br>\r\nNuova area MINICLUB con l\'orsotunnell e i cavalcabili<br>\r\nSpazio giochi con flipper e calciobalilla<br>\r\nConsole WII, dvd-teca e biblioteca per bambini<br>\r\nAttività all\'aria aperta gratuite per bambini\r\n <br><br>\r\nPER GLI SPORTIVI\r\n<br><br>\r\nMountain bike in hotel, corsi ed escursioni in mtb<br>\r\nBike Hotel Service: deposito mountain bike, piccola manutenzione e lavaggio della bike, bike corner con informazioni e mappe percorsi<br>\r\nTrasporto bagaglio mountain bikers nel circuito DBB Dolomiti Brenta Bike (a pagamento)<br>\r\nConvenzioni con Centro Rafting e Centro wellness Spa a Dimaro<br>\r\nEscursioni guidate in montagna e nel Parco Naturale Adamello Brenta<br>\r\nBus navetta dell\'Hotel gratuito per Daolasa (partenza degli impianti sciistici)<br>\r\nDeposito sci con asciugascarponi<br>\r\nLavanderia per l’abbigliamento tecnico (a pagamento)<br>\r\nL’Hotel Vittoria è interamente  NON FUMATORI ed è accessibile alle persone con disabilità.<br>', 'Via Ponte di Ferro, 41<br><br>', '0463 974113', '0463 974600', 'info@hotelvittoria.info', 'www.hotelvittoria.info', '3', 'home.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `HISTORY`
--

CREATE TABLE `HISTORY` (
  `id` int(11) NOT NULL,
  `section` enum('history','adamello_brenta','dolomiti') NOT NULL,
  `title` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `HISTORY`
--

INSERT INTO `HISTORY` (`id`, `section`, `title`, `description`, `img`) VALUES
(0, 'history', 'Primi decenni del \'900', 'Attraverso il pensiero di autorevoli naturalisti e uomini di cultura si fa largo l\'idea di far nascere un parco a tutela dei territori compresi, in Trentino, tra il massiccio dell\'Adamello-Presanella e il Gruppo delle Dolomiti di Brenta. Seppure nella diversità di obiettivi e di delimitazioni geografiche, le proposte che a più riprese si susseguono individuavano tre principali elementi di protezione: la Val Genova, il Gruppo di Brenta, in particolare il Lago di Tovel e l\'ultima popolazione autoctona di orso bruno delle Alpi.', ''),
(0, 'adamello_brenta', 'Adamello Brenta Geopark', 'Il 26 giugno 2008, a Osnabrueck, il Parco Naturale Adamello Brenta ha ricevuto il riconoscimento come Adamello Brenta Geopark, entrando a far parte della Rete Europea e Globale dei Geoparchi. Le due reti lavorano insieme per conservare e valorizzare il proprio patrimonio geologico sotto l’egida dell’Organizzazione delle Nazioni Unite per l’educazione, la scienza e la cultura, l’Unesco appunto.\r\n<br>\r\nIl riconoscimento come Geoparco testimonia la ricchezza e la straordinarietà del patrimonio geologico dell\'Adamello Brenta Geopark e conferma la validità del lavoro svolto finora dal Parco per lo sviluppo sostenibile del territorio.\r\n<br>\r\nL\'Adamello Brenta Geopark comprende l\'intera superficie del Parco Naturale Adamello Brenta e il territorio dei comuni a esso afferenti, andando a ricoprire un\'area di 1146kmq.', 'immagine-geoparco.jpg'),
(0, 'dolomiti', 'Dolomiti Patrimonio dell\'Umanità', 'Il 26 giugno 2009, a Siviglia, l’Unesco ha decretato l’inserimento delle Dolomiti nel patrimonio naturale mondiale dell’Umanità. Il riconoscimento corona un lungo cammino, di oltre quattro anni, che ha sancito lo straordinario valore paesaggistico e naturalistico delle “montagne più belle del mondo”. <br>\r\nA Siviglia, dopo la relazione illustrativa del proponente delegato dall’Union for Conservation of Nature (Iucn), gli interventi di approfondimento dell’ambasciatore italiano all’Unesco e la discussione da parte dei 21 componenti del World Hermitage Committee (Whc), la candidatura delle Dolomiti ha finalmente trovato accoglimento pieno.\r\nI nove gruppi dolomitici – Pelmo-Croda da Lago, Marmolada, Pale di San Martino-San Lucano, Dolomiti Bellunesi, Dolomiti Friulane e d\'Oltre Piave, Dolomiti Settentrionali, Puez-Odle, Sciliar-Catinaccio- Latemar, Bletterbach e Dolomiti di Brenta – hanno ottenuto il riconoscimento dall’organizzazione delle Nazioni unite per l’Educazione, la Scienza e la Cultura come bene seriale per la loro eccezionalità geologica e paesaggistica. Da oggi essi sono iscritti nella lista dei beni naturali dell’Unesco e dovranno sottostare a precise regole di tutela e di valorizzazione.\r\n', 'patrimonio-umanità.jpg'),
(1, 'history', '1967', 'Viene individuata l\'area protetta "Parco Naturale Adamello Brenta". La Provincia Autonoma di Trento la include, insieme al Parco Naturale Paneveggio Pale di San Martino, nel primo Piano urbanistico provinciale (Pup). Di fatto nascono i primi due parchi naturali d\'Italia, ma bisogna attendere ancora un ventennio prima che il Parco sia messo in condizione di "camminare con le proprie gambe". In questa fase la gestione del Parco rimane in capo alla Provincia che, tramite il Servizio parchi, attua per anni una politica di blanda valorizzazione naturalistica. Regge bene, tuttavia, il vincolo urbanistico che salvaguarda il territorio dalle discutibili speculazioni edilizie che, in quegli anni, interessano praticamente tutto il resto del territorio provinciale.', ''),
(2, 'history', '1987', 'Il nuovo Pup amplia considerevolmente i confini dell\'area protetta, portando la superficie protetta dai 504 kmq originari a 618 kmq.', ''),
(3, 'history', '1988', 'Viene promulgata la Legge provinciale 6 maggio 1988, n. 18, "Ordinamento dei parchi naturali", una legge moderna e anticipatrice dei principi di partecipazione poi sanciti dalla legge quadro nazionale 394/91. Essa definisce le finalità dei parchi naturali trentini come "la tutela delle caratteristiche naturali e ambientali, la promozione dello studio scientifico e l\'uso sociale dei beni ambientali" e stabilisce l\'organizzazione amministrativa e le linee generali di gestione dell\'area protetta.\r\n<br>\r\nCarlo Eligio Valentini è il primo Presidente del Parco (1988-1995)\r\n<br>\r\nSandro Flaim è il primo Direttore del Parco (1988-1994)', ''),
(4, 'history', '1994', 'Arrigo Franceschi Direttore del Parco (1994-2000)', ''),
(5, 'history', '1995', 'Antonello Zulberti Presidente del Parco (1995-2010)', ''),
(6, 'history', '1999', 'Dopo i primi anni molto difficili a livello di accettazione sociale, in cui il Parco deve trincerarsi nella difesa dei propri principi, nel 1999 l\'Adamello Brenta si dota di un fondamentale strumento gestionale: il Piano del Parco. La sua approvazione segna la fine della fase di opposizione sociale, consentendo così all\'area protetta di liberare le proprie energie in un\'azione finalmente propositiva.', ''),
(7, 'history', '2000', 'Claudio Ferrari Direttore del Parco (2000-2010)', ''),
(8, 'history', '2003', 'Con la revisione del Piano urbanistico provinciale, avvenuta nel 2003, i confini del Parco sono stati nuovamente ampliati, portando la superficie protetta agli attuali 620,52 kmq.', ''),
(9, 'history', '2006', 'Registrazione ambientale comunitaria EMAS con numero di registrazione IT-000576 e adesione alla Carta Europea del Turismo Sostenibile. ', ''),
(10, 'history', '2007', 'Viene promulgata la legge Provinciale sulle foreste e sulla protezione della natura (L.P. 23 maggio 2007, n. 11) "Governo del territorio forestale e montano, dei corsi d\'acqua e delle aree protette" che sostituisce e integra la L.P. 18/88. ', ''),
(11, 'history', '2008', 'Il Parco ha ottenuto il riconoscimento come <a href="#geopark">Adamello Brenta Geopark</a>, entrando a far parte della Rete Europea e Globale dei Geoparchi: un network di aree protette che lavorano insieme per valorizzare il patrimonio geologico della Terra sotto l’egida dell’Unesco.', ''),
(12, 'history', '2009', 'Il Gruppo dolomitico di Brenta con i suoi castelli di pietra con torri, campanili, ardite guglie e immani pareti... è entrato a far parte del <a href="#patrimonio">Patrimonio naturale Mondiale dell’Umanità</a>.', ''),
(13, 'history', '2010', 'Antonio Caola Presidente del Parco (2010-2015)', ''),
(14, 'history', '2011', 'Roberto Zoanetti Direttore del Parco (2011-2016)', ''),
(15, 'history', '2012', 'La sezione EMAS Italia nella seduta del Comitato per l’Ecolabel e l’Ecoaudit ha nuovamente deliberato il rinnovo della registrazione EMAS del Parco per il periodo 2012-2016', ''),
(16, 'history', '2013', 'Inaugurazione antica Segheria Veneziana di Dimaro nuovo Punto Info del Parco. Avvio del iter di adozione del nuovo Piano Territoriale.', ''),
(17, 'history', '2014', 'Inaugurata la nuova Casa del Parco Naturale Adamello Brenta AcquaLife situata nel Comune di Spiazzo lungo le rive del Sarca Il Parco Naturale Adamello Brenta apre a Carisolo la nuova Casa dedicata alla geologia e al Geoparco.', ''),
(18, 'history', '2015', 'Il Parco Naturale Adamello Brenta entra a far parte dei 120 geoparchi riconosciuti "UNESCO Global Geoparks".\r\n<br>\r\nAvv. Joseph Masè Presidente del Parco (2015-oggi)', ''),
(19, 'history', '2016', 'Al Parco Naturale Adamello Brenta l\'Oscar dell’Ecoturismo di Legambiente.', '');

-- --------------------------------------------------------

--
-- Table structure for table `PICTURES-ABOUTUS`
--

CREATE TABLE `PICTURES-ABOUTUS` (
  `id` varchar(191) NOT NULL,
  `num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `PICTURES-ABOUTUS`
--

INSERT INTO `PICTURES-ABOUTUS` (`id`, `num`) VALUES
('img1.jpg', 1),
('img2.jpg', 2),
('img3.jpg', 3),
('img4.jpg', 4),
('img5.jpg', 5),
('img6.jpg', 6),
('img7.jpg', 7),
('img8.jpg', 8);

-- --------------------------------------------------------

--
-- Table structure for table `PICTURES-HH`
--

CREATE TABLE `PICTURES-HH` (
  `id` varchar(191) NOT NULL,
  `num` int(11) NOT NULL,
  `reference` varchar(191) NOT NULL,
  `caption` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `PICTURES-HH`
--

INSERT INTO `PICTURES-HH` (`id`, `num`, `reference`, `caption`) VALUES
('alphotel-milano-estate-dolomiti.jpg', 0, 'alphotel_milano', 'L\'hotel\r\n'),
('alpholiday-dolomiti-wellness (1).jpg', 0, 'holiday_dolomiti', 'L\'hotel'),
('hotel-denny.jpg', 0, 'hotel_denny', 'L\'hotel'),
('hotel-gianna-img1.jpg', 0, 'hotel_gianna', 'L\'hotel\r\n'),
('estate_239315.jpeg', 0, 'hotel_regina_elena', 'L\'hotel\r\n'),
('Hotel_benessere_famiglie_Val_di_Fiemme.jpg', 0, 'hotel_rio', 'L\'hotel'),
('invernog_wellness_Hotel_Tevini__Campo_Base__val_di_Sole__Trentino_28c61cb9ed.jpg', 0, 'hotel_tevini', 'L\'hotel'),
('F_20302_333597.jpg', 0, 'hotel_vittoria', 'L\'hotel'),
('tramonto-paesaggio-innevato-andalo.jpg', 1, 'alphotel_milano', 'Il tramonto'),
('pisc_est_dsc_4692-a.jpeg', 1, 'holiday_dolomiti', 'Il girardino'),
('36674450.jpg', 1, 'hotel_denny', 'L\'hotel'),
('hotel-gianna-img2.jpg', 1, 'hotel_gianna', 'Il giardino'),
('regina_elena_inverno_est,11993.jpg', 1, 'hotel_regina_elena', 'L\'inverno'),
('hotel-rio-bianco.jpg', 1, 'hotel_rio', 'La piscina'),
('vacanza_relax_estate.jpg', 1, 'hotel_tevini', 'Il giardino'),
('1523-sport-hotel-vittoria-panorama776138.jpg', 1, 'hotel_vittoria', 'Le piste'),
('sci.jpg', 2, 'alphotel_milano', 'L\'inverno'),
('1dc69871519a098a5f727708428c214a.jpg', 2, 'holiday_dolomiti', 'L\'hotel'),
('ristorante.jpg', 2, 'hotel_denny', 'Il ristorante'),
('hotel-gianna-img3.jpg', 2, 'hotel_gianna', 'Il centro benessere'),
('41407677.jpg', 2, 'hotel_regina_elena', 'Il bar'),
('1.jpg', 2, 'hotel_rio', 'L\'inverno'),
('TRENTIN s.r.l.-a5156cba-09aa-457f-92a1-7ee360b53919.jpg', 2, 'hotel_tevini', 'Il centro benessere'),
('G.H. Sas di Toller Agostino & C.-04084324-dc94-4f52-8953-0cfbc41899f0.jpg', 2, 'hotel_vittoria', 'Il centro benessere'),
('cavallo-dolomiti-brenta.jpg', 3, 'alphotel_milano', 'L\'esterno'),
('alpholiday-dolomiti-wellness-fun-hotel-trentino-dimaro1.600.jpg', 3, 'holiday_dolomiti', 'Per i bambini'),
('filename-bar-jpg-thumbnail0.jpg', 3, 'hotel_denny', 'Il bar'),
('hotel-gianna-img4.jpg', 3, 'hotel_gianna', 'Il ristorante'),
('41407336.jpg', 3, 'hotel_regina_elena', 'Il ristorante'),
('01.jpg', 3, 'hotel_rio', 'La piscina'),
('abe4461530637d9315bbf7718ac2981946792502.jpeg', 3, 'hotel_tevini', 'Il bar'),
('camera-dalbergo-familiare_med.png', 3, 'hotel_vittoria', 'Le camere'),
('camera-hotel-andalo.jpg', 4, 'alphotel_milano', 'Le camere'),
('king-restaurant-alpholiday-dolomiti_295880.jpeg', 4, 'holiday_dolomiti', 'Il ristorante'),
('12769074.jpg', 4, 'hotel_denny', 'Il giardino'),
('hotel-gianna-img5.jpg', 4, 'hotel_gianna', 'Le camere'),
('41190383.jpg', 4, 'hotel_regina_elena', 'Le camere'),
('Esterno-Notturna-Hotel-Rio-Stava-600x400.jpg', 4, 'hotel_rio', 'L\'esterno'),
('superior-room.jpg', 4, 'hotel_tevini', 'Le camere'),
('wellness_hotel_vittoria_val_di_sole_dimaro_trentino.jpg', 4, 'hotel_vittoria', 'Il giardino'),
('vacanza-relax-hotel-andalo.jpg', 5, 'alphotel_milano', 'Le camere'),
('734.jpg', 5, 'holiday_dolomiti', 'Le camere'),
('photos_138581_1343036.jpg', 5, 'hotel_denny', 'Le camere'),
('hotel-gianna-img6.jpg', 5, 'hotel_gianna', 'Le camere'),
('camera-mansardata_239324.jpeg', 5, 'hotel_regina_elena', 'Le camere'),
('download.jpg', 5, 'hotel_rio', 'Le camere'),
('53668748.jpg', 5, 'hotel_tevini', 'Le camere'),
('2620_z_SportHotel_Vittoria_Tonale_Ristorane_G.jpg', 5, 'hotel_vittoria', 'Il ristorante');

-- --------------------------------------------------------

--
-- Table structure for table `PICTURES-TRAILS`
--

CREATE TABLE `PICTURES-TRAILS` (
  `id` varchar(191) NOT NULL,
  `num` int(11) NOT NULL,
  `reference` varchar(191) NOT NULL,
  `caption` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `PICTURES-TRAILS`
--

INSERT INTO `PICTURES-TRAILS` (`id`, `num`, `reference`, `caption`) VALUES
('laghi-di-cornisello-e-panoramica-sulla-cima-presanella.jpg', 0, 'amola_cornisello', 'Valina d\'Amola - Cornisello'),
('baite-di-clemp.jpg', 0, 'baite_clemp', 'Baite di Clemp'),
('lago-ritorto.jpg', 0, 'canton_di_ritorto', 'Canton di Ritorto'),
('le-famose-cascate-nardis-in-val-genova.jpg', 0, 'cascate_val_genova', 'Cascate di Val Genova'),
('le-famose-cascate-nardis-in-val-genova.jpg', 0, 'cascate_val_genova_diff', 'Cascate di Val Genova'),
('val-del-chiese-da-cima-durmont.jpg', 0, 'cima_durmont', 'Cima Durmont'),
('vista-panoramica-sulle-dolomiti-dal-lago-ritorto.jpg', 0, 'cinque_laghi', 'Giro dei 5 laghi'),
('dolomiti-di-brenta-dal-doss-del-sabion.jpg', 0, 'doss_sabion', 'Doss del Sabion'),
('sentiero-per-l-eremo-di-san-martino.jpg', 0, 'eremo_san_martino', 'Eremo di San Martino'),
('piazza-righi-madonna-di-campiglio.jpg', 0, 'giro_campiglio', 'Giro di Campiglio'),
('rifugio-maria-e-alberto-ai-brentei-dolomiti-di-brenta.jpg', 0, 'giro_rifugi', 'Giro dei rifugi'),
('chiesetta-ai-laghi-di-san-giuliano.jpg', 0, 'laghi_san_giuliano', 'Laghi di San Giuliano'),
('lago-e-rifugio-nambino-visti-dall-alto.jpg', 0, 'lago_nambino', 'Lago di Nambino'),
('escursionista-al-lago-di-valagola.jpg', 0, 'lago_valagola', 'Lago di Valagola'),
('campo-da-golf-a-campo-carlo-magno.jpg', 0, 'malghe_vagliana', 'Malghe Vagliana'),
('orti-della-regina.jpg', 0, 'orti_regina', 'Orti della Regina'),
('chiesetta-nella-roccia-in-prossimita-del-rifugio-xii-apostoli.jpg', 0, 'rifugio_garbari', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('sopra-il-rifugio-xii-apostoli-direzione-val-di-sacco.jpg', 0, 'rifugio_garbari_diff', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('rifugio-care-alto.jpg', 0, 'rifugio_ongati', 'Rifugio “Dante Ongari” al Caré alto'),
('rifugio-care-alto.jpg', 0, 'rifugio_ongati_diff', 'Rifugio “Dante Ongari” al Caré alto'),
('matarot-dal-centro-glaciologico-j-payer.jpg', 0, 'rifugio_trento', 'Rifugio "Città di Trento" al Mandrone'),
('rifugio-mandrone.jpg', 0, 'rifugio_trento_diff', 'Rifugio "Città di Trento" al Mandrone'),
('val-di-san-valentino.jpg', 0, 'san_valentino', 'Val di San Valentino'),
('il-crozzon-dalla-val-brenta.jpg', 0, 'val_brenta', 'Val Brenta'),
('cornisello-inferiore.jpg', 1, 'amola_cornisello', 'Valina d\'Amola - Cornisello'),
('forti-di-clemp.jpg', 1, 'baite_clemp', 'Baite di Clemp'),
('carrozza-trainata-da-cavalli-verso-malga-ritorto.jpg', 1, 'canton_di_ritorto', 'Canton di Ritorto'),
('ponte-in-prossimita-delle-cascate-di-lares.jpg', 1, 'cascate_val_genova', 'Cascate di Val Genova'),
('ponte-in-prossimita-delle-cascate-di-lares.jpg', 1, 'cascate_val_genova_diff', 'Cascate di Val Genova'),
('care-alto-e-corno-di-cavento-da-cima-durmont.jpg', 1, 'cima_durmont', 'Cima Durmont'),
('panorama-da-ritorto.jpg', 1, 'cinque_laghi', 'Giro dei 5 laghi'),
('doss-del-sabion.jpg', 1, 'doss_sabion', 'Doss del Sabion'),
('tra-faggi-e-gradini-in-pietra-lungo-il-sentiero-per-san-martino.jpg', 1, 'eremo_san_martino', 'Eremo di San Martino'),
('giro-di-campiglio-img1.jpg', 1, 'giro_campiglio', 'Giro di Campiglio'),
('rifugio-f-f-tuckett-ai-piedi-del-castelletto.jpg', 1, 'giro_rifugi', 'Giro dei rifugi'),
('laghi-di-san-giuliano.jpg', 1, 'laghi_san_giuliano', 'Laghi di San Giuliano'),
('vista-sulle-dolomiti-di-brenta-dal-rifugio-nambino.jpg', 1, 'lago_nambino', 'Lago di Nambino'),
('bambini-al-lago-di-valagola.jpg', 1, 'lago_valagola', 'Lago di Valagola'),
('laghetto-del-campo-di-golf-a-campo-carlo-magno.jpg', 1, 'malghe_vagliana', 'Malghe Vagliana'),
('orti-della-regina-vista-aerea.jpg', 1, 'orti_regina', 'Orti della Regina'),
('panoramica-sul-gruppo-adamello-presanella-dal-rifugio-xii-apostoli', 1, 'rifugio_garbari', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('rifugio-xii-apostoli-e-chiesetta-scavata-nella-roccia.jpg', 1, 'rifugio_garbari_diff', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('cima-care-alto.jpg', 1, 'rifugio_ongati', 'Rifugio “Dante Ongari” al Caré alto'),
('cima-care-alto.jpg', 1, 'rifugio_ongati_diff', 'Rifugio “Dante Ongari” al Caré alto'),
('cascate-del-matarot.jpg', 1, 'rifugio_trento', 'Rifugio "Città di Trento" al Mandrone'),
('salita-al-mandrone-dal-rifugio-bedole.jpg', 1, 'rifugio_trento_diff', 'Rifugio "Città di Trento" al Mandrone'),
('rifugio-gorck-in-val-di-san-valentino.jpg', 1, 'san_valentino', 'Val di San Valentino'),
('cascate-di-vallesinella.jpg', 1, 'val_brenta', 'Val Brenta'),
('biker-a-cornisello.jpg', 2, 'amola_cornisello', 'Valina d\'Amola - Cornisello'),
('vista-panoramica-della-val-rendena-dai-forti-di-clemp.jpg', 2, 'baite_clemp', 'Baite di Clemp'),
('lago-ritorto (2).jpg', 2, 'canton_di_ritorto', 'Canton di Ritorto'),
('cascata-di-lares.jpg', 2, 'cascate_val_genova', 'Cascate di Val Genova'),
('cascata-di-lares.jpg', 2, 'cascate_val_genova_diff', 'Cascate di Val Genova'),
('croce-sulla-cima-durmont.jpg', 2, 'cima_durmont', 'Cima Durmont'),
('lago-ritorto (1).jpg', 2, 'cinque_laghi', 'Giro dei 5 laghi'),
('pra-rodont-funivie-pinzolo.jpg', 2, 'doss_sabion', 'Doss del Sabion'),
('passerella-lungo-il-sentiero-per-l-eremo-di-san-martino.jpg', 2, 'eremo_san_martino', 'Eremo di San Martino'),
('giro-di-campiglio-img2.jpg', 2, 'giro_campiglio', 'Giro di Campiglio'),
('sentiero-per-il-rifugio-brentei.jpg', 2, 'giro_rifugi', 'Giro dei rifugi'),
('baite-a-malga-campo.jpg', 2, 'laghi_san_giuliano', 'Laghi di San Giuliano'),
('rifugio-lago-nambino1.jpg', 2, 'lago_nambino', 'Lago di Nambino'),
('lago-di-valagola.jpg', 2, 'lago_valagola', 'Lago di Valagola'),
('itinerario-vicino-al-campo-da-golf.jpg', 2, 'malghe_vagliana', 'Malghe Vagliana'),
('orti-della-regina (1).jpg', 2, 'orti_regina', 'Orti della Regina'),
('presanella-dai-xii-apostoli.jpg', 2, 'rifugio_garbari', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('rifugio-xii-apostoli.jpg', 2, 'rifugio_garbari_diff', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('val-san-valentino-bocca-conca-rifugio-care-alto.jpg', 2, 'rifugio_ongati', 'Rifugio “Dante Ongari” al Caré alto'),
('val-san-valentino-bocca-conca-rifugio-care-alto.jpg', 2, 'rifugio_ongati_diff', 'Rifugio “Dante Ongari” al Caré alto'),
('centro-glaciologico-julius-payer.jpg', 2, 'rifugio_trento', 'Rifugio "Città di Trento" al Mandrone'),
('sentiero-per-il-rifugio-mandrone.jpg', 2, 'rifugio_trento_diff', 'Rifugio "Città di Trento" al Mandrone'),
('alba-a-ritorto.jpg', 2, 'san_valentino', 'Val di San Valentino'),
('pra-de-la-casa-val-brenta.jpg', 2, 'val_brenta', 'Val Brenta'),
('panoramica-dall-alto-sui-laghi-di-cornisello.jpg', 3, 'amola_cornisello', 'Valina d\'Amola - Cornisello'),
('baite-di-clemp-e-dolomiti-di-brenta.jpg', 3, 'baite_clemp', 'Baite di Clemp'),
('alba-a-ritorto.jpg', 3, 'canton_di_ritorto', 'Canton di Ritorto'),
('ponte-lungo-il-sentiero-delle-cascate.jpg', 3, 'cascate_val_genova', 'Cascate di Val Genova'),
('ponte-lungo-il-sentiero-delle-cascate.jpg', 3, 'cascate_val_genova_diff', 'Cascate di Val Genova'),
('tratto-ripido-lungo-l-itinerario-per-cima-durmont.jpg', 3, 'cima_durmont', 'Cima Durmont'),
('escursionista-al-lago-di-valagola.jpg', 3, 'cinque_laghi', 'Giro dei 5 laghi'),
('seggiovia-da-pra-rodont-al-doss.jpg', 3, 'doss_sabion', 'Doss del Sabion'),
('passerella-e-cordini-d-acciaio-lungo-il-sentiero-per-l-eremo-di-san-martino.jpg', 3, 'eremo_san_martino', 'Eremo di San Martino'),
('giro-di-campiglio-img3.jpg', 3, 'giro_campiglio', 'Giro di Campiglio'),
('rifugio-vallesinella.jpg', 3, 'giro_rifugi', 'Giro dei rifugi'),
('care-alto-da-malga-san-giuliano.jpg', 3, 'laghi_san_giuliano', 'Laghi di San Giuliano'),
('anatroccoli-sul-lago-di-nambino.jpg', 3, 'lago_nambino', 'Lago di Nambino'),
('bikers-al-lago-di-valagola.jpg', 3, 'lago_valagola', 'Lago di Valagola'),
('val-d-algone-e-cima-vallon (1).jpg', 3, 'malghe_vagliana', 'Malghe Vagliana'),
('orti-della-regina-fossili.jpg', 3, 'orti_regina', 'Orti della Regina'),
('paesaggio-dei-xii-apostoli.jpg', 3, 'rifugio_garbari', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('paesaggio-dei-xii-apostoli.jpg', 3, 'rifugio_garbari_diff', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('cascate-di-vallesinella.jpg', 3, 'rifugio_ongati', 'Rifugio “Dante Ongari” al Caré alto'),
('cascate-di-vallesinella.jpg', 3, 'rifugio_ongati_diff', 'Rifugio “Dante Ongari” al Caré alto'),
('centro-glaciologico-julius-payer (1).jpg', 3, 'rifugio_trento', 'Rifugio "Città di Trento" al Mandrone'),
('centro-glaciologico-julius-payer (1).jpg', 3, 'rifugio_trento_diff', 'Rifugio "Città di Trento" al Mandrone'),
('bambini-al-lago-di-valagola.jpg', 3, 'san_valentino', 'Val di San Valentino'),
('attività-estive.jpg', 3, 'val_brenta', 'Val Brenta'),
('cornisello-superiore.jpg', 4, 'amola_cornisello', 'Valina d\'Amola - Cornisello'),
('resti-dei-forti-di-clemp.jpg', 4, 'baite_clemp', 'Baite di Clemp'),
('malga-ritorto.jpg', 4, 'canton_di_ritorto', 'Canton di Ritorto'),
('tratto-finale-della-val-genova-con-vista-sull-anfiteatro-delle-lobbie-e-del-mandrone.jpg', 4, 'cascate_val_genova', 'Cascate di Val Genova'),
('tratto-finale-della-val-genova-con-vista-sull-anfiteatro-delle-lobbie-e-del-mandrone.jpg', 4, 'cascate_val_genova_diff', 'Cascate di Val Genova'),
('val-d-algone-e-cima-vallon.jpg', 4, 'cima_durmont', 'Cima Durmont'),
('rifugio-lago-nambino.jpg', 4, 'cinque_laghi', 'Giro dei 5 laghi'),
('pra-de-la-casa-val-brenta.jpg', 4, 'doss_sabion', 'Doss del Sabion'),
('ponticello-sul-rio-di-san-martino-lungo-il-sentiero-per-l-omonima-chiesetta.jpg', 4, 'eremo_san_martino', 'Eremo di San Martino'),
('giro-di-campiglio-img1.jpg', 4, 'giro_campiglio', 'Giro di Campiglio'),
('cornisello-superiore.jpg', 4, 'giro_rifugi', 'Giro dei rifugi'),
('chiesa-di-santa-cilicia-ai-laghi-di-san-giuliano.jpg', 4, 'laghi_san_giuliano', 'Laghi di San Giuliano'),
('panoramica-lago-di-nambino.jpg', 4, 'lago_nambino', 'Lago di Nambino'),
('cascate-di-vallesinella.jpg', 4, 'lago_valagola', 'Lago di Valagola'),
('masi-di-malga-campo-all-imbrunire.jpg', 4, 'malghe_vagliana', 'Malghe Vagliana'),
('croce-sulla-cima-durmont.jpg', 4, 'orti_regina', 'Orti della Regina'),
('attorno-al-rifugio-xii-apostoli.jpg', 4, 'rifugio_garbari', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('attorno-al-rifugio-xii-apostoli.jpg', 4, 'rifugio_garbari_diff', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('matarot-e-lobbie-dal-sentiero-per-il-mandrone.jpg', 4, 'rifugio_ongati', 'Rifugio “Dante Ongari” al Caré alto'),
('matarot-e-lobbie-dal-sentiero-per-il-mandrone.jpg', 4, 'rifugio_ongati_diff', 'Rifugio “Dante Ongari” al Caré alto'),
('matarot-e-lobbie-dal-sentiero-per-il-mandrone.jpg', 4, 'rifugio_trento', 'Rifugio "Città di Trento" al Mandrone'),
('matarot-e-lobbie-dal-sentiero-per-il-mandrone.jpg', 4, 'rifugio_trento_diff', 'Rifugio "Città di Trento" al Mandrone'),
('campo-da-golf-a-campo-carlo-magno.jpg', 4, 'san_valentino', 'Val di San Valentino'),
('malga-san-giuliano.jpg', 4, 'val_brenta', 'Val Brenta'),
('le-acque-cristalline-di-cornisello-dove-si-specchia-la-presanella.jpg', 5, 'amola_cornisello', 'Valina d\'Amola - Cornisello'),
('vecchia-scala-delle-baite-di-clemp.jpg', 5, 'baite_clemp', 'Baite di Clemp'),
('scala-prima-dei-pascoli-di-san-giuliano.jpg', 5, 'canton_di_ritorto', 'Canton di Ritorto'),
('ponte-nei-pressi-di-malga-matarot.jpg', 5, 'cascate_val_genova', 'Cascate di Val Genova'),
('ponte-nei-pressi-di-malga-matarot.jpg', 5, 'cascate_val_genova_diff', 'Cascate di Val Genova'),
('val-del-chiese-e-lago-d-idro-da-cima-durmont.jpg', 5, 'cima_durmont', 'Cima Durmont'),
('nei-pressi-di-malga-campo.jpg', 5, 'cinque_laghi', 'Giro dei 5 laghi'),
('panorama-da-malga-san-giuliano.jpg', 5, 'doss_sabion', 'Doss del Sabion'),
('passaggio-con-cordini-lungo-il-sentiero-per-l-eremo-di-san-martino.jpg', 5, 'eremo_san_martino', 'Eremo di San Martino'),
('giro-di-campiglio-img2.jpg', 5, 'giro_campiglio', 'Giro di Campiglio'),
('baite-di-clemp.jpg', 5, 'giro_rifugi', 'Giro dei rifugi'),
('lago-e-rifugio-di-san-giuliano.jpg', 5, 'laghi_san_giuliano', 'Laghi di San Giuliano'),
('lago-e-rifugio-nambino.jpg', 5, 'lago_nambino', 'Lago di Nambino'),
('rifugio-e-lago-di-san-giuliano.jpg', 5, 'lago_valagola', 'Lago di Valagola'),
('cascate-di-vallesinella.jpg', 5, 'malghe_vagliana', 'Malghe Vagliana'),
('tratto-ripido-lungo-l-itinerario-per-cima-durmont.jpg', 5, 'orti_regina', 'Orti della Regina'),
('salita-al-rifugio-xii-apostoli-dalla-madonnina.jpg', 5, 'rifugio_garbari', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('salita-al-rifugio-xii-apostoli-dalla-madonnina.jpg', 5, 'rifugio_garbari_diff', 'Rifugio “Fratelli Garbari” ai XII apostoli'),
('rifugio-bedole.jpg', 5, 'rifugio_ongati', 'Rifugio “Dante Ongari” al Caré alto'),
('rifugio-bedole.jpg', 5, 'rifugio_ongati_diff', 'Rifugio “Dante Ongari” al Caré alto'),
('rifugio-bedole.jpg', 5, 'rifugio_trento', 'Rifugio "Città di Trento" al Mandrone'),
('rifugio-bedole.jpg', 5, 'rifugio_trento_diff', 'Rifugio "Città di Trento" al Mandrone'),
('cornisello-inferiore.jpg', 5, 'san_valentino', 'Val di San Valentino'),
('cascate-di-vallesinella.jpg', 5, 'val_brenta', 'Val Brenta');

-- --------------------------------------------------------

--
-- Table structure for table `RULES`
--

CREATE TABLE `RULES` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `RULES`
--

INSERT INTO `RULES` (`id`, `description`, `img`) VALUES
(1, 'Rispetta i fiori e le piante: i loro colori ed i loro profumi sono lì anche per chi passerà dopo di te.\r\n', 'RTEmagicC_vignetta_osvaldo_con_fiori.gif'),
(2, 'Quando avvisti degli animali, tieniti a distanza: rischi di spaventarli. Hanno paura anche dei cani che, per questo, è necessario tenere sempre sotto controllo.\r\n', 'RTEmagicC_vignetta_osvaldo_con_cane.gif'),
(3, 'Anche gli anfibi, i rettili e gli invertebrati (insetti, molluschi ecc.) sono fondamentali nell\'equilibrio della natura e meritano il tuo rispetto.', 'RTEmagicC_vignetta_osvaldo_con_rana.gif'),
(4, 'Il peggior nemico dei boschi è il fuoco. Se vuoi accenderne uno, usa i punti fuoco predisposti dal Parco o un apposito focolare ad almeno 50 metri dal limite del bosco.', 'RTEmagicC_vignetta_osvaldo_con_fuoco.gif'),
(5, 'Nel rispetto dei fiori, degli animali e degli escursionisti a piedi, usa la bicicletta solo sulle strade. Sui sentieri, spingila.\r\n', 'RTEmagicC_vignetta_osvaldo_con_bici.gif'),
(6, 'Minerali e fossili vanno lasciati dove sono. Sono lì da milioni di anni (anche grazie al rispetto di chi ti ha preceduto).', 'RTEmagicC_vignetta_osvaldo_con_fossili.gif'),
(7, 'Smorza i toni e accendi i tuoi sensi. Il Parco è tutt’altra musica.\r\n', 'RTEmagicC_vignetta_osvaldo_con_musica.gif'),
(8, 'Per non turbare il magico equilibrio del Parco, non è possibile campeggiare.', 'RTEmagicC_vignetta_osvaldo_con_tende.gif'),
(9, 'Quasi ovunque si possono raccogliere i funghi, ma serve un permesso, che viene rilasciato dai Comuni.', 'RTEmagicC_vignetta_osvaldo_con_funghi.gif'),
(10, 'Se ti capita di trovare qualche residuato bellico, non raccoglierlo; è vietato e potrebbe essere anche pericoloso. Piuttosto segnala il ritrovamento al Parco.', 'RTEmagicC_vignetta_osvaldo_con_reb.gif');

-- --------------------------------------------------------

--
-- Table structure for table `TRAILS`
--

CREATE TABLE `TRAILS` (
  `id` varchar(191) NOT NULL,
  `name` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `difficulty` enum('easy','moderate','challenging','difficult','very_difficult','extreme') NOT NULL,
  `img` varchar(191) NOT NULL,
  `distance` decimal(2,0) NOT NULL,
  `last` varchar(191) NOT NULL,
  `altitude` int(11) NOT NULL,
  `rate` enum('1','2','3','4','5','6') NOT NULL DEFAULT '1',
  `period` varchar(191) NOT NULL,
  `equipment` varchar(191) NOT NULL,
  `beginning` text NOT NULL,
  `ending` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `TRAILS`
--

INSERT INTO `TRAILS` (`id`, `name`, `description`, `difficulty`, `img`, `distance`, `last`, `altitude`, `rate`, `period`, `equipment`, `beginning`, `ending`) VALUES
('amola_cornisello', 'Valina d\'Amola - Cornisello', 'L\'itinerario si sviluppa in alta quota, fuori dalla vegetazione; si consiglia di affrontarlo preferibilmente nelle prime ore delle giornata e con condizioni metereologiche stabili.', 'challenging', 'laghi-di-cornisello-e-panoramica-sulla-cima-presanella.jpg', '9', '5:00', 515, '3', 'da giugno a settembre', 'si raccomandano scarponcini, un copricapo per il sole, giacca impermeabile e crema solare.', 'Dalla Val Nambrone lungo strada asfaltata fino al bivio per “Valina d’Amola” (1.916 m)<br><br>\r\nCoordinate:<br> \r\nGeogr. 46.207235 N 10.736842 E <br>\r\nUTM 32T 633983 5118539', 'Parcheggio “Valina d’Amola”\r\n<br><br>\r\nDal piccolo parcheggio di "Valina d\'Amola" s’imbocca a sinistra la strada bianca che porta in breve, verso sud, al torrente “Sarca d’Amola”, segnavia 211. Prima di attraversare il ponte si gira a destra e si prende il sentiero che sale costeggiando il torrente. Di nuovo su strada forestale ci si trova in breve a Malga Valina d’Amola.<br>\r\nSi gira a destra (verso ovest), si attraversa il torrente su un ponticello in legno e al bivio si volta di nuovo a destra, verso nord. Si inizia a salire zigzagando il ripido costone fino a raggiungere la sommità del crinale, che si percorre fino a raggiungere il Rifugio Segantini.<br>\r\nSi oltrepassa il rifugio, segnavia 216, si attraversa il torrente e si sale sul culmine di una dorsale, dominati dalla maestosità della Cima Presanella. Da qui, abbassandosi repentinamente, ci si porta sulla “Costiera di Cornisello” e la si attraversa in quota fino a raggiungere la “Bocchetta dell’Om”, che offre una stupenda vista sui sottostanti Laghi di Cornisello.<br>\r\nSi scende ripidamente fino ad incontrare un bivio, dove si devia a destra imboccando il sentiero con segnavia 238 sino a raggiungere il Lago Nero.\r\nLo si supera e si prosegue, ancora in discesa, fino al Rifugio Cornisello.\r\nContinuando, invece, sul sentiero numero 211 è possibile raggiungere e costeggiare i Laghi di Cornisello, per poi giungere in prossimità del rifugio. Dal Rifugio, seguendo la strada della Val Nambrone, si ritorna al piccolo parcheggio da dove si è partiti.'),
('baite_clemp', 'Baite di Clemp e forti di guerra', 'L\'itinerario, indicato per gli amanti della tranquillità e del passato bellico della Val Rendena, si sviluppa prevalentemente in un bosco misto di latifoglie e conifere offrendo un\'incantevole vista panoramica su tutta la Val Rendena, i ghiacciai dell’Adamello e le Dolomiti di Brenta. L\'escursione concilia aspetti naturalisti e storico-etnografici conducendo alla scoperta di interessanti resti della Prima Guerra Mondiale, tra cui il Forte Clemp, avamposto Austriaco durante la Prima Guerra Mondiale. Lungo l\'itinerario si possono inoltre ammirare notevoli esempi di "Case da Mont", baite in tradizionale stile architettonico montano della Val Rendena.', 'difficult', 'baite-di-clemp.jpg', '6', '3:30', 593, '4', 'da maggio a ottobre', 'si raccomandano scarponcini, copricapo e giacca impermeabile.', 'Da Sant’Antonio di Mavignola “Parco ai Sass” (1.167 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.192559 N 10.789939 E <br>\r\nUTM 32T 638116 5116999<br>', '“Parco ai Sass”\r\n<br><br>\r\nDal Parco ai Sass si prende a destra la strada forestale che porta in quota nella “Val dal Cridac”.<br>\r\nDopo alcuni tornanti si attraversa il rio e ci si sposta sulla sinistra orografica. Proseguendo si giunge all’acquedotto da dove, finita la strada, si imbocca il sentiero.<br>\r\nSi sale il crinale della montagna zigzagando, fino ad arrivare ai resti di una fortificazione austroungarica utilizzata nella prima guerra mondiale.<br> Dopo poco si arriva al punto panoramico delle “Baite di Clemp”, da dove si può godere di una vista meravigliosa sulla Val Rendena e sulla Cima Presanella.<br>\r\nSi prosegue su strada carrozzabile, verso sud, e si inizia la discesa verso Sant’Antonio di Mavignola.<br>\r\nLungo la strada, che presenta anche tratti pendenti, si incontra il caratteristico percorso della Via Crucis; si prosegue fino ai prati sopra il paese, da dove si ritorna in piano al “Parco ai Sass”.'),
('canton_di_ritorto', 'Canton di Ritorto', 'L\'escursione al bellissimo lago di Ritorto è indicata per famiglie e ragazzi dai 10-12 anni. Suggeriamo una pausa a Malga Ritorto. Si consiglia di portare da bere: non sono presenti punti di ristoro per lunghi tratti di itinerario. Considerata la lunghezza del percorso, suggeriamo di partire presto in mattinata.', 'moderate', 'lago-ritorto.jpg', '13', '5:30', 600, '2', 'da maggio a ottobre', 'si raccomandano scarponcini, un copricapo per il sole e giacca impermeabile.', 'da Madonna di Campiglio al termine di via Adamello (ristorante Pappagallo) (1.544 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.224669 N 10.823093 E <br>\r\nUTM 32T 640592 5120625<br>', 'Madonna di Campiglio<br><br>\r\nDal ristorante Pappagallo (in fondo a via Adamello) si imbocca il sentiero del Giro di Campiglio in direzione sud e lo si percorre fino ad una biforcazione, alla quale si svolta a destra.\r\nSi inizia a salire nel fitto bosco per un primo tratto con ampie curve e, successivamente, con pendenze meno accentuate fino a raggiungere il pascolo di Malga Ritorto.<br>\r\nSi sale lungo la strada forestale incontrando la casina, lo stallone e, in seguito, Rifugio Ritorto (è possibile raggiungere il Rifugio in auto o in trenino partendo da Patascoss).<br>\r\nDal Rifugio si prende a sinistra, sempre per stradina sterrata, e ci si inoltra nella Val dei Cantòn fino ad imboccare il sentiero che si inerpica nel fitto bosco. Si attraversa il Rio Colarìn e, giunti in una zona pianeggiante, si supera un bivio tenendosi a sinistra per poi affrontare l’ultimo dislivello fino ad arrivare al Lago Ritorto (il Lago può essere raggiunto più comodamente salendo in quota con la Cabinovia 5 Laghi e percorrendo il breve sentiero pianeggiante). Dal lago ci si sposta verso sud per imboccare il sentiero con segnavia 277, ci si porta subito a Malga Pozza dei Garzoni e con successivi saliscendi si raggiunge il culmine del Pian dei Mughi. Qui, al bivio dei sentieri, si gira a sinistra mantenendo il medesimo segnavia e si inizia la discesa. Si ritorna nel bosco e con ampie curve ci si abbassa fino a trovarsi nuovamente a Malga Ritorto.\r\nSi prosegue scendendo in direzione sud ora per strada forestale evitando di riprendere il sentiero percorso in salita e mantenendosi sulla destra fino a raggiungere Casa Capòt.<br>\r\nSi prende a sinistra verso est e si imbocca nel bosco il sentiero, che con tranquilla discesa arriva alla località Panorama.<br>\r\nOra, verso nord, si prende il percorso denominato Sentiero dei Siori, si cammina in piano e ci si riporta a Madonna di Campiglio, al punto di partenza dell’itinerario.'),
('cascate_val_genova', 'Cascate della Val Genova', 'Si consiglia una sosta in uno dei tanti e accoglienti rifugi della valle. E\' possibile ridurre l\'escursione usufruendo del servizio di bus navetta che costeggia l\'itinerario lungo la strada asfaltata principale. Informati sugli orari del bus navetta e sulle norme che regolano la viabilità prima di partire.', 'difficult', 'le-famose-cascate-nardis-in-val-genova.jpg', '26', '10:00', 1638, '4', 'da maggio a settembre', 'si raccomandano scarponcini, un copricapo per il sole, giacca impermeabile e crema solare.', 'da Carisolo percorrendo la Val Genova fino alla località Ponte Verde. (923 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.168870 N 10.721216 E <br>\r\nUTM 32T 632871 5114250', 'Ponte Verde\r\n<br><br>\r\nSi imbocca il sentiero ben segnalato e curato, che si sposta subito sulla sponda destra orografica del torrente “Sarca di Val Genova” e si raggiunge il ristorante di fronte all’imponente “Cascata Nardis”, con il suo spettacolare salto verticale di oltre 130 metri. Si prosegue nel bosco di conifere lungo un tratto in saliscendi, si attraversa il torrente che scende dalla “Cascata di Siniciaga”e si continua con percorso quasi pianeggiante fino alla “Valle di Lares”, sotto le omonime cascate.<br>\r\nSi percorrono i pascoli della “Malga Genova” fino a rientrare sulla strada del fondovalle, si attraversa “Ponte Maria” e si prosegue in sinistra orografica superando un tratto ripido fino alla località “Todesca”.<br>\r\nSi riattraversa la strada portandosi di nuovo in destra orografica del torrente - località “Ragada” - e si imbocca il sentiero con segnavia 207, che attraversa tutto il pianoro fino a raggiungere il “Rifugio Stella Alpina”.<br>\r\nIl sentiero prosegue tra il torrente “Sarca” e la strada fino a raggiungere le “Cascate del Pedruc”; da qui si continua lungo la strada fino al pascolo pianeggiante di “Malga Bedole”.<br>\r\nSi rientra percorrendo a ritroso il percorso descritto.<br>\r\nNumerose sono le possibilità di variante all’itinerario proposto, considerando anche il servizio navetta che viene proposto durante l’estate lungo la Val Genova.\r\n'),
('cascate_val_genova_diff', 'Cascate della Val Genova', 'Si consiglia una sosta in uno dei tanti e accoglienti rifugi della valle. E\' possibile ridurre l\'escursione usufruendo del servizio di bus navetta che costeggia l\'itinerario lungo la strada asfaltata principale. Informati sugli orari del bus navetta e sulle norme che regolano la viabilità prima di partire.', 'very_difficult', 'le-famose-cascate-nardis-in-val-genova.jpg', '26', '10:00', 1638, '5', 'da maggio a settembre', 'si raccomandano scarponcini, un copricapo per il sole, giacca impermeabile e crema solare.', 'da Carisolo percorrendo la Val Genova fino alla località Ponte Verde. (923 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.168870 N 10.721216 E <br>\r\nUTM 32T 632871 5114250', 'Ponte Verde\r\n<br><br>\r\nSi imbocca il sentiero ben segnalato e curato, che si sposta subito sulla sponda destra orografica del torrente “Sarca di Val Genova” e si raggiunge il ristorante di fronte all’imponente “Cascata Nardis”, con il suo spettacolare salto verticale di oltre 130 metri. Si prosegue nel bosco di conifere lungo un tratto in saliscendi, si attraversa il torrente che scende dalla “Cascata di Siniciaga”e si continua con percorso quasi pianeggiante fino alla “Valle di Lares”, sotto le omonime cascate.<br>\r\nSi percorrono i pascoli della “Malga Genova” fino a rientrare sulla strada del fondovalle, si attraversa “Ponte Maria” e si prosegue in sinistra orografica superando un tratto ripido fino alla località “Todesca”.<br>\r\nSi riattraversa la strada portandosi di nuovo in destra orografica del torrente - località “Ragada” - e si imbocca il sentiero con segnavia 207, che attraversa tutto il pianoro fino a raggiungere il “Rifugio Stella Alpina”.<br>\r\nIl sentiero prosegue tra il torrente “Sarca” e la strada fino a raggiungere le “Cascate del Pedruc”; da qui si continua lungo la strada fino al pascolo pianeggiante di “Malga Bedole”.<br>\r\nSi rientra percorrendo a ritroso il percorso descritto.<br>\r\nNumerose sono le possibilità di variante all’itinerario proposto, considerando anche il servizio navetta che viene proposto durante l’estate lungo la Val Genova.\r\n'),
('cima_durmont', 'Cima Durmont', 'Passeggiata di media difficoltà, molto apprezzata per il fantastico panorama che spazia dal Carè Alto alla Presanella, alle Dolomiti di Brenta. La cima, posta all’estremità sudoccidentale del Gruppo Brenta, è raggiungibile da comodo sentiero, quasi interamente nel bosco. Solo l\'ultimo tratto è su terreno un pò sconnesso e piuttosto esposto. Vale la pena affrontare il percorso nel periodo primaverile o autunnale quando il bosco si tinge di mille colori.\r\nSi consiglia di portare da bere e, considerata la lunghezza e l\'esposizione del percorso, suggeriamo di partire presto in mattinata.', 'challenging', 'val-del-chiese-da-cima-durmont.jpg', '10', '5:00', 719, '3', 'da giugno a settembre', 'si raccomandano scarponcini, copricapo e giacca impermeabile.', 'Capanna Durmont - Passo Daone (1.305 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.078687 N 10.745493 E <br>\r\nUTM 32T 634965 5104271', 'Capanna Durmont - Passo Daone\r\n<br><br>\r\nDalla “Capanna Durmont” ci si dirige verso nord e, dopo circa cento metri, si prende a destra la strada carrozzabile.\r\nSuperata un’area picnic si svolta a destra, si imbocca il sentiero che attraversa il bosco e si raggiunge la strada forestale che sale da “Binio” in località “Dotor”.<br>\r\nSi segue quest’ultima fino ai masi di “Pramarciù” e si prosegue sempre in salita, ora più accentuata, fino a raggiungere località “Stavel”\r\nPoco sopra si riprende verso destra il sentiero che gradualmente esce dalla vegetazione d’alto fusto alle pendici del crinale sud della cima. Si prosegue inerpicandosi per il sentiero fino ad oltrepassare la Croce di Vetta raggiungendo così la “Cima Durmont”.<br>\r\nSi prosegue per cresta verso nord abbassandosi progressivamente di quota fino al “Passo Campiol o Malghette” da dove, a destra (versante “Val Manez”), si prende il tratturo che dopo poco diventa strada forestale.<br>\r\nSi scende per quest’ultima evitando un bivio a destra fino a raggiungere la località “Stablei”; si passa un tornante e ci si dirige verso la località “Manez”.<br>\r\nSi superano i pascoli ed al loro limitare ci si tiene a destra, sempre su strada, in modo da completare a ritroso la traversata del versante fino a riportarsi in località “Dotor” e poi al “Passo Daone”, da dove si è partiti.'),
('cinque_laghi', 'Giro dei 5 laghi', 'L\'itinerario è un vero classico che si sviluppa lungo le pendici del gruppo della Presanella. Il sentiero, pur essendo comodo e molto frequentato, richiede un\'adeguata preparazione fisica per lo sviluppo e la durata dell’escursione. br>\r\nSi consiglia di portare da bere: non sono presenti punti di ristoro per lunghi tratti di itinerario. Considerata la lunghezza del percorso, suggeriamo di partire presto in mattinata.', 'moderate', 'vista-panoramica-sulle-dolomiti-dal-lago-ritorto.jpg', '11', '6:00', 932, '2', 'da giugno a settembre', 'si raccomandano scarponcini, un copricapo per il sole e giacca impermeabile.', 'da Madonna di Campiglio si raggiunge la località Patascoss (1.727 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.230945 N 10.815936 E <br>\r\nUTM 32T 640025 5121310 <br>', 'Patascoss\r\n<br><br>\r\nDal parcheggio di Patascoss, dopo poche decine di metri sulla strada per Malga Ritorto, si devia a destra e ci si incammina in salita lungo la strada forestale.<br>\r\nSalendo si incontra prima la baita Casinel ed infine alcuni tornanti che portano al Rifugio Pancugolo, adiacente all’arrivo della Cabinovia 5 Laghi (è possibile raggiungere il Rifugio Pancugolo direttamente in telecabina, partendo dal centro di Madonna di Campiglio). Imboccando ora il sentiero che parte dietro al Rifugio si raggiunge lungo percorso pianeggiante il Lago Ritorto (il lago di Ritorto può essere raggiunto partendo anche da Malga Ritorto passando per i “Cantoni di Ritorto”).<br>\r\nDal lago il sentiero inizia a salire in direzione nord fino a raggiungere il Passo di Ritorto, oltre il quale si prosegue con leggeri sali scendi tra massi e lastroni. Evitando la deviazione che porta direttamente al Lago Nambino, si raggiunge il Lago Lambin. Si prosegue risalendo il costone che sovrasta la Val Nambino e poi, scendendo, fino al Lago Serodoli.<br>\r\nDalla bocca del lago ci si innalza oltrepassando la casetta del bivacco e si raggiunge in breve il vicino Lago Gelato.<br>\r\nRitornando sui propri passi si raggiunge di nuovo la bocca del Lago Serodoli e si discende il tratto impegnativo che porta al piccolo Lago Nero. Successivamente si incontra un bivio, si prosegue a sinistra lungo il sentiero con segnavia 266 in direzione “Busa dei Cavai”. La si supera, si scende per alcuni tornanti con vista sul Lago Nambino e si taglia il pendio abbassandosi gradualmente fino a raggiungere il lago stesso e l’omonimo Rifugio.<br>\r\nNei pressi della bocca del lago si prende a destra per il sentiero con il quale, dopo un tratto pianeggiante che taglia in quota il versante est del Monte Pancugolo, si ritorna al punto di partenza di Patascoss.'),
('doss_sabion', 'Doss del Sabion', 'Luogo ideale per un pic-nic in famiglia. Parco giochi, gonfiabili e tappeti elastici sono allestiti all\'arrivo delle telecabina, a Prà Rodont.<br>\r\nL\'itinerario può essere accorciato tagliandone alcuni tratti o utilizzando la seggiovia.', 'moderate', 'dolomiti-di-brenta-dal-doss-del-sabion.jpg', '11', '5:00', 736, '2', 'da giugno a settembre', 'si raccomandano scarponcini, copricapo e giacca impermeabile.', 'da Pinzolo si sale in località Prà Rodont (raggiungibile in cabinovia) (1.504 m)\r\nCoordinate: \r\nGeogr. 46.167328 N 10.786041 E \r\nUTM 32T 637879 5114189', 'Prà Rodont\r\n<br><br>\r\nDall’arrivo della cabinovia si imbocca il sentiero che risale lungo la pista da sci in direzione nord (pista Fossadei) e si prosegue camminando fino alla caratteristica Malga Cioca e al vicino ristorante. Si prosegue in direzione nord-est, ora per via forestale con segnavia 357, si aggira la montagna fino ad incontrare Malga Grual, da dove si inizia a salire più decisamente. In breve ci si porta su un pianoro in corrispondenza della partenza delle seggiovie e si prosegue lungo la strada che gira verso destra in direzione ovest. Si continua fino al termine della stessa e si imbocca il sentiero con segnavia 357b; lo si percorre salendo lungo la cresta nord del “Doss del Sabion” fino a raggiungerne la cima. E’ possibile variare l’itinerario giungendo qui direttamente con l’impianto di risalita. Dal Rifugio Doss del Sabion si continua in discesa percorrendo il sentiero denominato “Soffio del Vento” prima lungo la pista da sci (Grual) e poi lungo tutto il crinale verso est (evitando di prendere direttamente la traccia che lo taglia). Si raggiunge un bivio, dove si svolta a destra (direzione sud) e si torna a percorrere il sentiero con segnavia 357, che porta al Passo Bregn da l’Ors attraverso un panoramico saliscendi con vista sulle Dolomiti di Brenta e sul sottostante Lago di Valagola.\r\nAl crocevia dei sentieri posto in località “Madonnina” si prende a destra verso ovest, segnavia 307, e dopo aver superato la “Baita dei cacciatori” si torna su strada bianca; la si percorre in discesa attraversando i pascoli fino a raggiungere Malga Bregn da l’Ors. Si abbandona la strada e si gira verso destra imboccando il sentiero “Fratelli Tartarotti” con segnavia 307b; si supera un ponticello e si procede in piano per tutto il pascolo. Entrati nel bosco si inizia a scendere gradualmente, si evita un bivio sulla sinistra e si procede fino a tornare in località Prà Rodont.'),
('eremo_san_martino', 'Eremo di San Martino', 'L\'eremo, a 1226 metri di quota, è noto per la leggenda popolare che lo vedeva abitato da uno degli ultimi eremiti. Secondo la fantasia popolare l\'eremita viveva del pane che gli portava un orso mansueto e la sua morte fu annunciata miracolosamente dagli avornielli (fiori di maggiociondolo) che fiorirono a gennaio.<br>\r\nItinerario dal grande fascino soprattutto nei mesi primaverili ed autunnali quando il bosco indossa colori forti e vivaci. <br>\r\nNon presentando particolari difficoltà, se non un un paio di tratti un pò esposti ma ben protetti da cordini, il percorso è indicato anche per bambini a partire dagli 8/10 anni.', 'moderate', 'l-eremo-di-san-martino-sopra-carisolo.jpg', '4', '2:30', 440, '2', 'da aprile a ottobre', 'si raccomandano scarponcini, copricapo e giacca impermeabile.', 'Carisolo centro (814 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.169634 N 10.762297 E <br>\r\nUTM 32T 636040 5114404', 'Carisolo<br><br>\r\nDa Piazza 2 Maggio si salgono le gradinate che portano al sagrato della Chiesa e, superatolo, si cammina per alcune centinaia di metri lungo la strada che si dirige in Val Genova.\r\nSi imbocca a destra la deviazione che porta verso la località “Campolo”; al primo tornante si incontra un bivio, da dove ha inizio il sentiero con segnavia 228.<br>\r\nSi attraversa il bosco salendo lungo il sentiero con pendenza accentuata, si arriva alla “Gola del Re” e si supera un passaggio un po’ esposto (attrezzato con cavo metallico).<br>\r\nSi prosegue risalendo le ultime rampe del costone roccioso ricoperto da abeti, larici e faggi fino a raggiungere il piccolo Eremo di San Martino. Si ritorna verso valle percorrendo a ritroso il medesimo itinerario.'),
('giro_campiglio', 'Giro di Campiglio', 'L\'itinerario, semplice e dai trascurabili dislivelli, è particolarmente indicato per famiglie con bambini o escursionisti agli esordi.\r\n<br>\r\nPuò essere affrontato anche con condizioni meteo incerte perchè è possibile abbandonarlo velocemente da più punti con sbocco in paese. Alcuni tratti dell\'itinerario sono comodi per spostarsi da Campiglio nord a Campiglio sud o da Campiglio alla partenza della cabinovia del Grostè evitando la strada asfaltata principale. ', 'easy', 'piazza-righi-madonna-di-campiglio.jpg', '7', '02:30', 200, '1', 'da marzo a novembre.', 'si consigliano scarponcini o scarpe da trekking.', 'Dal paese si accede al percorso da diverse posizioni. Da tutti i punti d’accesso è possibile completare l’anello. (1.610 m)\r\n<br>\r\nCoordinate:\r\n<br>\r\nGeogr. 46.234910 N 10.830162 E\r\n<br>\r\nUTM 32T 641111 5121776', 'Diversi punti di uscita con sbocco a Madonna di Campiglio.\r\n<br>\r\n<br>\r\nDalla Conca Verde, nel centro di Campiglio, si raggiunge la partenza della Cabinovia Spinale, si supera il Palacampiglio e si prosegue lungo la strada asfaltata. Girando a sinistra ci si porta alla Casa delle Regole, da dove si entra nel bosco. Poco dopo si esce dallo stesso e si prosegue in discesa tra le case fino alla strada di Vallesinella.\r\n<br>\r\nLa si attraversa e si ritorna a scendere, anche ripidamente, fino a raggiungere il torrente Sarca, che si oltrepassa su ponte di legno.\r\n<br>\r\nRisalita la ripida sponda in destra orografica ci si porta in piano fino alla strada e poi, tagliando per alcuni boschetti dietro le case, si sale fino alla strada principale.\r\n<br>\r\nSi continua a salire, prima costeggiando il rio Colarin e, in seguito, nel bosco oltre la strada per località Panorama, fino ad incrociare il Sentiero dei Siori, che si percorre fino all’uscita in via Adamello. La si percorre fino alla Caserma dei Carabinieri, si svolta a sinistra superando alcuni gradini e ci si porta di nuovo nel bosco.\r\n<br>\r\nSi prosegue incontrando la Cabinovia 5 Laghi, il Canalone Miramonti e, seguito un tratto di pista da sci, l’ingresso nord della galleria.\r\n<br>\r\nRiattraversando il fiume Sarca ed incrociata la Cabinovia Pradalago ci si porta sulla strada per la Val Nambino, da dove prima si sale leggermente e poi si prosegue in piano fino ad incontrare la pista da sci che scende da Pradalago.\r\n<br>\r\nSi scende imboccando il tunnel della stessa fino al Rio Grotte da dove, tenendosi a sinistra sul sentiero, ci si porta fino alle prime case e, di seguito, su strada asfaltata fino al punto di partenza.\r\n<br>\r\nE’ possibile, come variante a quanto proposto, percorrere anche parzialmente il percorso grazie ai diversi punti d’accesso presenti. '),
('giro_rifugi', 'Giro dei rifugi', 'Escursione classica, di media difficoltà e lunga percorrenza, con vista mozzafiato sulle Dolomiti e sul gruppo dell\'Adamello-Presanella. Imperdibile per gli amanti delle Dolomiti e della vita in rifugio.\r\nSi consiglia di portare da bere.<br> Considerata la lunghezza del percorso, suggeriamo di partire presto in mattinata.\r\nI tratti di sentiero esposti sono ben protetti con cordini di sicurezza (non è necessario l\'equipaggiamento da ferrata), e il canale, spesso innevato, in prossimità del Rifugio Brentei, viene normalmente battuto dai manutentori del sentiero.', 'extreme', 'rifugio-maria-e-alberto-ai-brentei-dolomiti-di-brenta.jpg', '15', '6:00', 1028, '6', 'da giugno a settembre', 'si raccomandano scarponcini, copricapo, crema solare e giacca impermeabile.', 'da Madonna di Campiglio partendo dalla località Palù, si raggiunge il Rifugio Vallesinella (1.526 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.206814 N 10.851829 E <br>\r\nUTM 32T 642855 5118693', 'Rifugio Vallesinella\r\n<br><br>\r\nDal piazzale del Rifugio Vallesinella si sale mantenendosi a sinistra, direzione nord, per un centinaio di metri fino a raggiungere la partenza della teleferica.<br>\r\nLasciando la strada sterrata si imbocca a sinistra il sentiero 382, si sale gradualmente e si raggiunge il pascolo della Malga Vallesinella di Sopra.\r\nLo si attraversa e in prossimità dell’incrocio si lascia il segnavia 382 per imboccare il sentiero 317b. Oltrepassata la Malga si continua a salire nel bosco guadagnando dolcemente quota fino a raggiungere il Rifugio Casinei.<br>\r\nSi prende a sinistra il sentiero 317; dopo un primo tratto abbastanza pianeggiante si sale decisamente superando prima il tratto di sentiero denominato Calvario del Tuckett e, successivamente, le rampe che portano al Rifugio Tuckett (il Rifugio è raggiungibile, in alternativa, dal Passo del Grostè utilizzando l’omonima Cabinovia e poi seguendo il sentiero 316).<br>\r\nDal Rifugio Tuckett si torna verso valle per qualche decina di metri e poi si prosegue verso est imboccando il sentiero 328. Si attraversa un tratto di sentiero che passa tra gradi massi e si raggiunge in discesa la Sella del Fredolin, caratteristico prato tra la Vallesinella e la Val Brenta.<br>\r\nContinuando in discesa tra i mughi si raggiunge il sentiero 318, che sale direttamente da Vallesinella.<br>\r\nSi imbocca in salita il sentiero, denominato Arnaldo Bogani, che segue con evidenti sali scendi i pendii occidentali della Val Brenta.<br>\r\nSi superano alcuni punti esposti ma protetti da cavo d’acciaio, la galleria scavata nella roccia ed un canalone (spesso innevato) ed in breve ci si trova alla piana dei Brentei ed all’omonimo Rifugio Maria ed Alberto.\r\nSi è nel cuore delle Dolomiti di Brenta, dove si può vivere il fascino dell’alpinismo e della sua storia.<br>\r\nPer il rientro si percorre a ritroso il sentiero 318 e, lasciandosi alle spalle il crocevia con il sentiero 328, si scende gradualmente lungo il sentiero fino a raggiungere il Rifugio Casinei. Da qui si scende direttamente, con pendenze più accentuate, al Rifugio Vallesinella per il sentiero 317.'),
('laghi_san_giuliano', 'Laghi di San Giuliano', 'Si consiglia una sosta all\'accogliente rifugio di San Giuliano, ristrutturato recentemente, e alla vicina chiesetta-santuario in riva al lago. La chiesetta, dedicata a S. Giuliano di Cilicia, pare fosse custodita un tempo da un eremita oltre ad essere celebrata nell’antichità per la sua fonte di “acqua buona per le febbri”. <br>\r\nNon dimenticare la macchina fotografica: l\'itinerario offre una splendida visuale sulla cima Presanella, sui ghiacciai del Carè Alto-Adamello e sulle Dolomiti di Brenta.', 'challenging', 'chiesetta-ai-laghi-di-san-giuliano.jpg', '12', '6:00', 959, '3', 'da maggio a settembre', 'si raccomandano scarponcini, un copricapo per il sole, giacca impermeabile e crema solare.', 'Da Caderzone Terme a Malga Campo (1.412 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.159229 N 10.739649 E <br>\r\nUTM 32T 634317 5113210<br>', 'Malga Campo\r\n<br><br>\r\nDa Malga Campo, luogo con una bellissima vista sulla Cima Presanella, si imbocca il sentiero 230. Si attraversa in quota il versante in destra orografica della Val di Genova fino alla valletta da cui si raggiungono i pascoli di Malga San Giuliano. Dopo breve discesa, si prosegue fino all’ omonimo lago, la piccola chiesa ed il rifugio. Poco distante si trova il lago Garzonè, separato dal primo dall’azione di riempimento dei detriti morenici.\r\nSi continua ora in salita, seguendo il segnavia 221, fino alla “Bocchetta dell’Acqua Fredda” o “Passo San Giuliano” da dove si possono ammirare le Dolomiti di Brenta in tutta la loro bellezza.<br>\r\nSi scende con ampie curve una valletta per giungere prima al Lago di Vacarsa e poi a Malga Campostril.<br>\r\nSi supera la malga mantenendo il medesimo segnavia in direzione Caderzone Terme; dopo pochi minuti di camino si svolta a sinistra verso località “Pozza delle Vacche”, da dove si ritrova la strada forestale che porta in leggera salita a Malga Campo.\r\n'),
('lago_nambino', 'Lago di Nambino', 'Posto ideale per famiglie con bambini e pescatori. Sulle sponde dell\'omonimo lago, il rifugio Nambino offre pranzi, cene e soggiorni in un ambiente accogliente e suggestivo. Rilascio permessi per la pesca presso il rifugio.', 'challenging', 'lago-e-rifugio-nambino-visti-dall-alto.jpg', '5', '9:00', 243, '3', 'da maggio a ottobre', 'si raccomandano scarponcini, un copricapo per il sole e giacca impermeabile.', 'Piana di Nambino (1.650 m)<br>\r\nCoordinate: <br>\r\nGeogr. 46.238550 N 10.818410 E <br>\r\nUTM 32T 640196 5122159', 'Piana di Nambino\r\n<br><br>\r\nSi percorre tutta la Piana di Nambino seguendo la strada sterrata in direzione nord fino alla partenza della teleferica, da dove si imbocca il sentiero a destra, segnavia 217.<br>\r\nSi sale quest’ultimo prima per un tratto di mulattiera, poi per percorso più movimentato costeggiando il torrente. Superato un tratto ripido si raggiunge in breve il Lago Nambino e l’omonimo Rifugio. E’ possibile raggiungere la sponda opposta del lago spostandosi un centinaio di metri prima del Rifugio ed attraversando il ponte in legno.<br>\r\nPer il rientro è possibile seguire il sentiero pianeggiante nel bosco fino a Malga Patascoss; da qui si prende la strada forestale che porta in discesa alla Piana di Nambino.'),
('lago_valagola', 'Lago di Valagola', 'La facile escursione, particolarmente indicata per famiglie con bambini, si sviluppa su strada forestale e tratti di sentiero tra boschi e prati. Attraversata la bella foresta del Cantin, i cui caratteri sono quelli di un bosco alpino allo stato vergine, si raggiunge il solitario lago di Valagola sulle cui sponde erbose si affaccia la rigogliosa foresta di abeti mentre nelle verdi acque del lago si specchiano cime maestose.<br>\r\nLuogo ideale per riposarsi e godere del silenzio e la pace dei boschi. Data l\'assenza di punti di ristoro, si consiglia di portarsi da bere.', 'easy', 'escursionista-al-lago-di-valagola.jpg', '8', '03:00', 511, '1', 'da maggio a ottobre.', 'si raccomandano scarponcini, un copricapo per il sole e giacca impermeabile.', 'Parcheggio in Valagola (1.163 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.192419 N 10.790972 E <br>\r\nUTM 32T 638196 5116985<br>', 'Parcheggio in Valagola\r\n<br>\r\n<br>\r\nDa S. Antonio di Mavignola si segue la strada per la Val Brenta, si prosegue fino ad un evidente bivio dove si imbocca, sulla destra, la Valagola. Si raggiunge il parcheggio e si segue la strada bianca che conduce al Lago di Valagola ed all’omonima malga.<br>\r\nIl ritorno si effettua per la stessa via.'),
('malghe_vagliana', 'Malghe Vagliana ', 'L\'itinerario, poco impegnativo e a tratti su strada sterrata, è particolarmente adatto a famiglie con bambini. La Malga Mondifrà, l\'unica con servizio di ristorazione lungo il percorso, offre pranzi e cene tipiche. Prenotazione consigliata.', 'easy', 'campo-da-golf-a-campo-carlo-magno.jpg', '9', '03:38', 378, '1', 'da aprile a ottobre.', 'si raccomandano scarpe da trekking o scarponcini e giacca impermeabile.\r\n', 'Loc. Fortini (presso partenza cabinovia Grostè) (1.647 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.236964 N 10.835632 E <br>\r\nUTM 32T 641528 5122014<br>', 'Loc. Fortini (presso partenza cabinovia Grostè)\r\n<br>\r\n<br>\r\nDalla località Fortini (Madonna di Campiglio - presso partenza cabinovia Grostè) si costeggia il campo da golf e si segue la comoda stradina fino a località Poza Vecia. Qui si prende a sinistra la strada forestale che conduce a Malga Vaglianella.\r\nLa si supera e si prosegue, in salita, fino al bellissimo pascolo di Malga Vagliana. Si ritorna sui propri passi fino alla Malga Vaglianella, si gira a destra, si giunge ai piedi della Val Gelada e si continua a scendere fino a Malga Mondifrà. Da qui si prosegue lungo la strada fino al punto di partenza.'),
('orti_regina', 'Orti della Regina', 'Itinerario poco impegnativo, di elevato interesse geologico e pregio paesaggistico. L\'escursione, un tempo meta prediletta della principessa Sissi e dell\'aristocrazia viennese, offre maestosi panorami sul gruppo di Brenta e l\'Adamello-Ortles ed è particolarmente suggestiva per via della presenza di fossili marini e la ricca varietà di flora alpina.', 'extreme', 'orti-della-regina.jpg', '5', '2:00', 470, '6', 'da giugno ad agosto', 'si raccomandano scarponcini, un copricapo per il sole, giacca impermeabile e crema solare.', 'Passo Grostè (2.440 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.214894 N 10.901190 E <br>\r\nUTM 32T 646641 5119680<br>', 'Rifugio Graffer\r\n<br><br>\r\nDal Passo Grostè (raggiungibile con la telecabina Grostè) si segue in discesa l’ampio sentiero che si snoda lungo la pista da sci ai piedi del massiccio della Pietra Grande. Giunti al Rifugio Graffer si svolta a destra, si costeggia il ghiaione e si risalgono facili roccette. Si raggiunge così una terrazza erbosa interessante per la presenza di fossili di antichissima origine e per la ricca varietà di fiori presenti.<br>\r\nSi rientra percorrendo lo stesso sentiero fino al Rifugio Graffer, per poi scendere a Campo Carlo Magno in cabinovia - dalla stazione \'intermedia del Grostè - o a piedi.'),
('rifugio_garbari', 'Rifugio “Fratelli Garbari” ai XII apostoli', 'L\'itinerario richiede una buona preparazione fisica e presenta tratti su ghiaioni e roccette; il più impegnativo, detto Scala Santa, è attrezzato con cavi d\'acciaio. La bellezza del luogo ripaga la fatica dell\'ascesa.<br>\r\nNei pressi del Rifugio vi è una chiesetta scavata nella roccia dedicata ai caduti della montagna che merita una visita.\r\nSe il programma prevede la discesa dal Doss in funivia, prestare attenzione all\'orario dell\'ultima corsa.', 'very_difficult', 'chiesetta-nella-roccia-in-prossimita-del-rifugio-xii-apostoli.jpg', '11', '5:00', 1081, '5', 'da giugno a settembre', 'si raccomandano scarponi da trekking, copricapo, giacca impermeabile e crema solare. Il kit da ferrata non è necessario.', 'Doss del Sabion (2.088 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.167231 N 10.806794 E <br>\r\nUTM 32T 639481 5114215', 'Doss del Sabion\r\n<br><br>\r\nDal paese di Pinzolo si raggiunge il Doss del Sabion utilizzando gli impianti di risalita (cabinovia e seggiovia). Dal Rifugio Doss del Sabion si continua in discesa percorrendo il sentiero denominato “Soffio del Vento” prima lungo la pista da sci e poi lungo tutto il crinale verso est; si prosegue tenendosi sulla sinistra e si scende in direzione nord. Si raggiunge un bivio, dove si svolta a destra (direzione sud) e si torna a percorrere il sentiero con segnavia 357, che porta al Passo Bregn da l’Ors e alla località Madonnina. Da qui si prende in direzione est per il comodo sentiero che dopo alcuni saliscendi porta al caratteristico pianoro denominato Piano di Nardis o Lago Asciutto. Lo si attraversa e si inizia a salire superando una zona rocciosa detta Scala Santa, attrezzata con cavi d’acciaio, e si continua per ghiaioni e roccette fino a giungere al Rifugio. Si rientra percorrendo a ritroso l’itinerario descritto.'),
('rifugio_garbari_diff', 'Rifugio “Fratelli Garbari” ai XII apostoli', 'L\'itinerario richiede una buona preparazione fisica e presenta tratti su ghiaioni e roccette; il più impegnativo, detto Scala Santa, è attrezzato con cavi d\'acciaio. La bellezza del luogo ripaga la fatica dell\'ascesa.<br>\r\nNei pressi del Rifugio vi è una chiesetta scavata nella roccia dedicata ai caduti della montagna che merita una visita.\r\nSe il programma prevede la discesa dal Doss in funivia, prestare attenzione all\'orario dell\'ultima corsa.', 'extreme', 'chiesetta-nella-roccia-in-prossimita-del-rifugio-xii-apostoli.jpg', '11', '5:00', 1081, '6', 'da giugno a settembre', 'si raccomandano scarponi da trekking, copricapo, giacca impermeabile e crema solare. Il kit da ferrata non è necessario.', 'Doss del Sabion (2.088 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.167231 N 10.806794 E <br>\r\nUTM 32T 639481 5114215', 'Doss del Sabion\r\n<br><br>\r\nDal paese di Pinzolo si raggiunge il Doss del Sabion utilizzando gli impianti di risalita (cabinovia e seggiovia). Dal Rifugio Doss del Sabion si continua in discesa percorrendo il sentiero denominato “Soffio del Vento” prima lungo la pista da sci e poi lungo tutto il crinale verso est; si prosegue tenendosi sulla sinistra e si scende in direzione nord. Si raggiunge un bivio, dove si svolta a destra (direzione sud) e si torna a percorrere il sentiero con segnavia 357, che porta al Passo Bregn da l’Ors e alla località Madonnina. Da qui si prende in direzione est per il comodo sentiero che dopo alcuni saliscendi porta al caratteristico pianoro denominato Piano di Nardis o Lago Asciutto. Lo si attraversa e si inizia a salire superando una zona rocciosa detta Scala Santa, attrezzata con cavi d’acciaio, e si continua per ghiaioni e roccette fino a giungere al Rifugio. Si rientra percorrendo a ritroso l’itinerario descritto.'),
('rifugio_ongati', 'Rifugio “Dante Ongari” al Caré alto', 'La fatica degli oltre 1200 metri di dislivello è completamente ripagata dalla bellezza selvaggia di un ambiente vario e maestoso e dalla posizione altamente panoramica.<br>\r\nL\'area dove sorge il rifugio e quella circostante era occupata, durante la Grande Guerra, da un vero e proprio "villaggio militare", rifornito da una fitta rete di linee teleferiche e tutta la zona è ricca di testimonianze belliche. Tra queste, merita una visita la "bocca del Cannone", a circa 2.850 metri, raggiungibile da un sentiero ben segnalato.', 'very_difficult', 'rifugio-care-alto.jpg', '9', '6:30', 1297, '5', 'da maggio a settembre', 'si raccomandano scarponi da trekking, giacca impermeabile, copricapo e crema solare.', 'Pian della Sega - Val di Borzago (1.229 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.102850 N 10.666300 E <br>\r\nUTM 32T 628785 5106824', 'Pian della Sega - Val di Borzago\r\n<br><br>\r\nDa Spiazzo Rendena si imbocca in auto la Val di Borzago fino al Pian della Sega. Lasciata la macchina si prende per strada sterrata nel bosco e si sale fino a Malga Coèl di Pelugo, da dove parte la teleferica per il Rifugio, segnavia 213. Si prende il sentiero nel bosco di latifoglie, che porta fino al ponte (Pont dal Zucal). Si continua a salire molto ripidamente fino a località Belvedere. Si costeggia con traccia più pianeggiante il costone sud del crinale portandosi verso la Val di Conca da dove, ancora ripidamente, si sale al Rifugio. Si rientra a valle scendendo a ritroso il percorso descritto.'),
('rifugio_ongati_diff', 'Rifugio “Dante Ongari” al Caré alto', 'La fatica degli oltre 1200 metri di dislivello è completamente ripagata dalla bellezza selvaggia di un ambiente vario e maestoso e dalla posizione altamente panoramica.<br>\r\nL\'area dove sorge il rifugio e quella circostante era occupata, durante la Grande Guerra, da un vero e proprio "villaggio militare", rifornito da una fitta rete di linee teleferiche e tutta la zona è ricca di testimonianze belliche. Tra queste, merita una visita la "bocca del Cannone", a circa 2.850 metri, raggiungibile da un sentiero ben segnalato.', 'extreme', 'rifugio-care-alto.jpg', '9', '6:30', 1297, '6', 'da maggio a settembre', 'si raccomandano scarponi da trekking, giacca impermeabile, copricapo e crema solare.', 'Pian della Sega - Val di Borzago (1.229 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.102850 N 10.666300 E <br>\r\nUTM 32T 628785 5106824', 'Pian della Sega - Val di Borzago\r\n<br><br>\r\nDa Spiazzo Rendena si imbocca in auto la Val di Borzago fino al Pian della Sega. Lasciata la macchina si prende per strada sterrata nel bosco e si sale fino a Malga Coèl di Pelugo, da dove parte la teleferica per il Rifugio, segnavia 213. Si prende il sentiero nel bosco di latifoglie, che porta fino al ponte (Pont dal Zucal). Si continua a salire molto ripidamente fino a località Belvedere. Si costeggia con traccia più pianeggiante il costone sud del crinale portandosi verso la Val di Conca da dove, ancora ripidamente, si sale al Rifugio. Si rientra a valle scendendo a ritroso il percorso descritto.'),
('rifugio_trento', 'Rifugio "Città di TrentO" al Mandrone', 'La salita è costante e piuttosto rigida all\'inizio: la prima parte del tragitto è immersa nel folto del bosco, poi, giunti a circa 2100 m, si esce dalla vegetazione per proseguire al cospetto della vedretta della Lobbia e panorami da togliere il fiato.<br>\r\nConsigliamo qualche tappa da non mancare a poca distanza dal rifugio: il Centro Glaciologico Julius Payer, mostra permanente sui ghiacciai e l\'ambiente montano; i laghetti omonimi e la piccola chiesetta costruita con il granito del posto.<br>\r\nNel corso della Guerra bianca la zona del Mandron fu teatro di scontri bellici e i dintorni del rifugio sono particolarmente interessanti perchè ancora ricchi di testimonianze della Grande Guerra: camminamenti, posti di vedetta, trincee ed il piccolo cimitero militare.', 'difficult', 'matarot-dal-centro-glaciologico-j-payer.jpg', '8', '5:30', 972, '4', 'da giugno a settembre', 'si raccomandano scarponcini, giacca impermeabile, copricapo e crema solare.', 'Malga Bedole - Val Genova (1.583 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.198231 N 10.603711 E <br>\r\nUTM 32T 623734 5117322', 'Malga Bedole - Val Genova\r\n<br><br>\r\nDa Malga Bedole in Val Genova ci si porta in breve al Rifugio Adamello Collini, da dove inizia il sentiero con segnavia 212. Ci si alza superando con numerosi tornanti il ripido costone, fino a portarsi nel circo superiore della valle. Oltrepassata la panca di “Mezza Via” e lasciato a destra l’imbocco per il sentiero Migotti, si procede con pendenze più lievi. <br>\r\nAppagati dalla magnifica veduta si raggiunge il vecchio rifugio, oggi Centro Glaciologico; da qui, in qualche minuto, si giunge al Rifugio Mandrone. Per il rientro si segue in discesa il medesimo sentiero.'),
('rifugio_trento_diff', 'Rifugio "Città di TrentO" al Mandrone', 'La salita è costante e piuttosto rigida all\'inizio: la prima parte del tragitto è immersa nel folto del bosco, poi, giunti a circa 2100 m, si esce dalla vegetazione per proseguire al cospetto della vedretta della Lobbia e panorami da togliere il fiato.<br>\r\nConsigliamo qualche tappa da non mancare a poca distanza dal rifugio: il Centro Glaciologico Julius Payer, mostra permanente sui ghiacciai e l\'ambiente montano; i laghetti omonimi e la piccola chiesetta costruita con il granito del posto.<br>\r\nNel corso della Guerra bianca la zona del Mandron fu teatro di scontri bellici e i dintorni del rifugio sono particolarmente interessanti perchè ancora ricchi di testimonianze della Grande Guerra: camminamenti, posti di vedetta, trincee ed il piccolo cimitero militare.', 'very_difficult', 'matarot-dal-centro-glaciologico-j-payer.jpg', '8', '5:30', 972, '5', 'da giugno a settembre', 'si raccomandano scarponcini, giacca impermeabile, copricapo e crema solare.', 'Malga Bedole - Val Genova (1.583 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.198231 N 10.603711 E <br>\r\nUTM 32T 623734 5117322', 'Malga Bedole - Val Genova\r\n<br><br>\r\nDa Malga Bedole in Val Genova ci si porta in breve al Rifugio Adamello Collini, da dove inizia il sentiero con segnavia 212. Ci si alza superando con numerosi tornanti il ripido costone, fino a portarsi nel circo superiore della valle. Oltrepassata la panca di “Mezza Via” e lasciato a destra l’imbocco per il sentiero Migotti, si procede con pendenze più lievi. <br>\r\nAppagati dalla magnifica veduta si raggiunge il vecchio rifugio, oggi Centro Glaciologico; da qui, in qualche minuto, si giunge al Rifugio Mandrone. Per il rientro si segue in discesa il medesimo sentiero.'),
('san_valentino', 'Val di San Valentino', 'L\'itinerario, piuttosto semplice, offre interessanti spunti culturali ed è indicato per famiglie e ragazzi dai 10-12 anni. Lungo il percorso consigliamo una visita alla chiesa di San Valentino, affrescata dai Baschenis, e una sosta al capitello votivo in località “Bressanina”. Da ammirare anche le tipiche “ca’ da mont”, tradizionali abitazioni in pietra e legno. Suggeriamo una pausa all\'accogliente rifugio Gorck che, costruito nel rispetto delle caratteristiche peculiari delle abitazioni della Valle di San Valentino, offre la possibilità di ristoro e pernottamento.', 'difficult', 'val-di-san-valentino.jpg', '6', '2:30', 443, '4', 'da aprile a ottobre', 'si raccomandano scarponcini, copricapo e giacca impermeabile.', 'Piazza Manzoni a Javrè (610 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.065973 N 10.713375 E <br>\r\nUTM 32T 632512 5102804', 'Javrè\r\n<br><br>\r\nDa piazza Manzoni a Javrè, dove si trova la sede della Cassa Rurale di Spiazzo-Javrè (da vedere, sulla facciata, l’opera pittorica realizzata nel 1986 da Gianluigi Rocca) si sale verso la parte ovest del paese e poi, lungo sentiero ben tracciato, verso il monte Nizzone e la suggestiva e panoramica chiesa di San Valentino (all’interno affreschi si Simone Baschenis). Da qui inizia il paesaggio di mezza quota della montagna rendenese, con le tipiche “ca’ da mont”, tradizionali abitazioni in pietra e legno circondate da ampi prati e pascoli. Poco più avanti, in coincidenza del capitello votivo in località “Bressanina”, si seguono le indicazioni per il rifugio Gorck, addentrandosi nella Val di San Valentino, una delle più interessanti del Parco Naturale Adamello Brenta. Dal rifugio Gorck si ritorna, lungo un percorso ad anello, per un altro sentiero che passa per Villa Rendena dopo aver lambito la forra di Cerion con l’omonima cascata sul rio Bedù.'),
('val_brenta', 'Val Brenta', 'La Val Brenta è un\'area incantevole e tranquilla. Oltre ad essere una delle principali vie d\'accesso al Gruppo di Brenta dalla Val Rendena, offre itinerari indicati per famiglie con bambini e ragazzi e i suoi prati sono ideali per per picnic e brevi passeggiate. <br>\r\nLa valle è circondata dalle cime più famose del Gruppo di Brenta: a sud ovest il Crozzon di Val d\'Agola, il Crozzon di Brenta, la Cima Tosa, la Cima Margherita, ad est la Brenta Alta e la Catena degli Sfulmini, a nord la Cima Mandron e le Punte di Campiglio. \r\nConcediti del tempo per ammirare il "sentiero delle Forra" che costeggia il fiume Sarca e il naturale canyon che disegna.', 'easy', 'il-crozzon-dalla-val-brenta.jpg', '6', '2:20', 250, '1', 'da marzo a novembre.', 'si raccomandano scarponi o scarpe da trekking a collo alto e giacca impermeabile.', 'Parcheggio in Val Brenta dopo località “Prà de la Casa” (1.238 m)<br><br>\r\nCoordinate: <br>\r\nGeogr. 46.199887 N 10.829219 E<br> \r\nUTM 32T 641128 5117883<br>', 'Parcheggio iniziale\r\n<br>\r\n<br>\r\nDal parcheggio si cammina lungo la strada sterrata, segnavia 323, si oltrepassa il ponte sul fiume Sarca di Brenta e si raggiunge il pascolo di Malga Brenta Bassa. Qui si è immersi nei più suggestivi e selvaggi scenari del Parco Adamello Brenta, dominati dall’imponenza del Crozzon di Brenta.\r\nCi si porta nei pressi del caseggiato della Malga e si continua procedendo in direzione est lungo la strada forestale che, oltre al ben visibile tornante, sale verso l’alto.<br>\r\nSi prosegue fino ad incontrare sulla sinistra il sentiero che, in breve, conduce alle Cascate di Mezzo di Vallesinella.<br>\r\nSi oltrepassa, tra gli spruzzi d’acqua, il ponticello in legno e si risale al vicino omonimo Rifugio. <br>\r\nSi prosegue in piano verso sinistra e dopo poco, girando di nuovo a sinistra, si prende la strada forestale e la si discende lasciandosi sulla destra l’imbocco del Sentiero dell’Arciduca per Madonna di Campiglio\r\nContinuando a scendere si giunge prima al “Prato del Forno” e, successivamente, nei pressi di Malga Fratte. Qui si prende a sinistra, si abbandona la strada inoltrandosi nel bosco, si riattraversa il fiume Sarca di Vallesinella e, in piano, si torna a Malga Brenta Bassa. <br>\r\nDa qui è possibile tornare al parcheggio prendendo il Sentiero della Forra, dove è possibile ammirare le forre d’acqua dai colori cristallini.');

-- --------------------------------------------------------

--
-- Table structure for table `TRAILS-HH`
--

CREATE TABLE `TRAILS-HH` (
  `trail` varchar(191) NOT NULL,
  `hh` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `TRAILS-HH`
--

INSERT INTO `TRAILS-HH` (`trail`, `hh`) VALUES
('amola_cornisello', 'alphotel_milano'),
('amola_cornisello', 'holiday_dolomiti'),
('amola_cornisello', 'hotel_denny'),
('amola_cornisello', 'hotel_gianna'),
('baite_clemp', 'hotel_regina_elena'),
('baite_clemp', 'hotel_rio'),
('baite_clemp', 'hotel_tevini'),
('baite_clemp', 'hotel_vittoria'),
('canton_di_ritorto', 'alphotel_milano'),
('canton_di_ritorto', 'holiday_dolomiti'),
('canton_di_ritorto', 'hotel_denny'),
('canton_di_ritorto', 'hotel_gianna'),
('cascate_val_genova_diff', 'hotel_regina_elena'),
('cascate_val_genova_diff', 'hotel_rio'),
('cascate_val_genova_diff', 'hotel_tevini'),
('cascate_val_genova_diff', 'hotel_vittoria'),
('cima_durmont', 'alphotel_milano'),
('cima_durmont', 'holiday_dolomiti'),
('cima_durmont', 'hotel_denny'),
('cima_durmont', 'hotel_gianna'),
('cinque_laghi', 'hotel_regina_elena'),
('cinque_laghi', 'hotel_rio'),
('cinque_laghi', 'hotel_tevini'),
('cinque_laghi', 'hotel_vittoria'),
('doss_sabion', 'alphotel_milano'),
('doss_sabion', 'holiday_dolomiti'),
('doss_sabion', 'hotel_denny'),
('doss_sabion', 'hotel_gianna'),
('eremo_san_martino', 'hotel_regina_elena'),
('eremo_san_martino', 'hotel_rio'),
('eremo_san_martino', 'hotel_tevini'),
('eremo_san_martino', 'hotel_vittoria'),
('giro_campiglio', 'alphotel_milano'),
('giro_campiglio', 'holiday_dolomiti'),
('giro_campiglio', 'hotel_denny'),
('giro_campiglio', 'hotel_gianna'),
('giro_rifugi', 'hotel_regina_elena'),
('giro_rifugi', 'hotel_rio'),
('giro_rifugi', 'hotel_tevini'),
('giro_rifugi', 'hotel_vittoria'),
('laghi_san_giuliano', 'alphotel_milano'),
('laghi_san_giuliano', 'holiday_dolomiti'),
('laghi_san_giuliano', 'hotel_denny'),
('laghi_san_giuliano', 'hotel_gianna'),
('lago_nambino', 'hotel_regina_elena'),
('lago_nambino', 'hotel_rio'),
('lago_nambino', 'hotel_tevini'),
('lago_nambino', 'hotel_vittoria'),
('lago_valagola', 'alphotel_milano'),
('lago_valagola', 'holiday_dolomiti'),
('lago_valagola', 'hotel_denny'),
('lago_valagola', 'hotel_gianna'),
('malghe_vagliana', 'hotel_regina_elena'),
('malghe_vagliana', 'hotel_rio'),
('malghe_vagliana', 'hotel_tevini'),
('malghe_vagliana', 'hotel_vittoria'),
('orti_regina', 'alphotel_milano'),
('orti_regina', 'holiday_dolomiti'),
('orti_regina', 'hotel_denny'),
('orti_regina', 'hotel_gianna'),
('rifugio_garbari', 'hotel_regina_elena'),
('rifugio_garbari', 'hotel_rio'),
('rifugio_garbari', 'hotel_tevini'),
('rifugio_garbari', 'hotel_vittoria'),
('rifugio_garbari_diff', 'hotel_regina_elena'),
('rifugio_garbari_diff', 'hotel_rio'),
('rifugio_garbari_diff', 'hotel_tevini'),
('rifugio_garbari_diff', 'hotel_vittoria'),
('rifugio_ongati', 'alphotel_milano'),
('rifugio_ongati', 'holiday_dolomiti'),
('rifugio_ongati', 'hotel_denny'),
('rifugio_ongati', 'hotel_gianna'),
('rifugio_ongati_diff', 'alphotel_milano'),
('rifugio_ongati_diff', 'holiday_dolomiti'),
('rifugio_ongati_diff', 'hotel_denny'),
('rifugio_ongati_diff', 'hotel_gianna'),
('rifugio_trento', 'hotel_regina_elena'),
('rifugio_trento', 'hotel_rio'),
('rifugio_trento', 'hotel_tevini'),
('rifugio_trento', 'hotel_vittoria'),
('rifugio_trento_diff', 'hotel_regina_elena'),
('rifugio_trento_diff', 'hotel_rio'),
('rifugio_trento_diff', 'hotel_tevini'),
('rifugio_trento_diff', 'hotel_vittoria'),
('san_valentino', 'alphotel_milano'),
('san_valentino', 'holiday_dolomiti'),
('san_valentino', 'hotel_denny'),
('san_valentino', 'hotel_gianna'),
('val_brenta', 'alphotel_milano'),
('val_brenta', 'holiday_dolomiti'),
('val_brenta', 'hotel_denny'),
('val_brenta', 'hotel_gianna');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ABOUTUS`
--
ALTER TABLE `ABOUTUS`
  ADD PRIMARY KEY (`id`,`section`);

--
-- Indexes for table `HH`
--
ALTER TABLE `HH`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `HISTORY`
--
ALTER TABLE `HISTORY`
  ADD PRIMARY KEY (`id`,`section`);

--
-- Indexes for table `PICTURES-ABOUTUS`
--
ALTER TABLE `PICTURES-ABOUTUS`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PICTURES-HH`
--
ALTER TABLE `PICTURES-HH`
  ADD PRIMARY KEY (`num`,`reference`),
  ADD KEY `reference` (`reference`);

--
-- Indexes for table `PICTURES-TRAILS`
--
ALTER TABLE `PICTURES-TRAILS`
  ADD PRIMARY KEY (`num`,`reference`),
  ADD KEY `reference` (`reference`);

--
-- Indexes for table `RULES`
--
ALTER TABLE `RULES`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TRAILS`
--
ALTER TABLE `TRAILS`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TRAILS-HH`
--
ALTER TABLE `TRAILS-HH`
  ADD PRIMARY KEY (`trail`,`hh`),
  ADD KEY `hh` (`hh`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `RULES`
--
ALTER TABLE `RULES`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `PICTURES-HH`
--
ALTER TABLE `PICTURES-HH`
  ADD CONSTRAINT `PICTURES-HH_ibfk_1` FOREIGN KEY (`reference`) REFERENCES `HH` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `PICTURES-TRAILS`
--
ALTER TABLE `PICTURES-TRAILS`
  ADD CONSTRAINT `PICTURES-TRAILS_ibfk_1` FOREIGN KEY (`reference`) REFERENCES `TRAILS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `TRAILS-HH`
--
ALTER TABLE `TRAILS-HH`
  ADD CONSTRAINT `TRAILS-HH_ibfk_1` FOREIGN KEY (`trail`) REFERENCES `TRAILS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `TRAILS-HH_ibfk_2` FOREIGN KEY (`hh`) REFERENCES `HH` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
