$(document).ready(ready);

function ready(){
    var id = getParameterByName('id');

    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getHH.php", //Relative or absolute path to file.php file
        data: {id:id},
        success: function(response) {
            //console.log(JSON.parse(response));
            var hh=JSON.parse(response);
            var title='';
            var pic='';
            var info='';
            var description='';
            var address='';
            var services='';
            for(var i=0;i<hh.length;i++){
                //console.log(hh[i].id);

                title+='<h1>' + hh[i].name + '</h1>'
                    +'<div class="ratings">';
                for(var j=0;j<hh[i].rate;j++){
                    title+='<span class="glyphicon glyphicon-star"></span>';
                }
                title+='</div> <br>'
                    +'<div class="line"></div> <br>';

                pic+='<img src="/img/' + hh[i].img + '">';

                info+='<h4>' + hh[i].name + '<br></h4>'
                    +'<h5>' + hh[i].address + '</h5>'
                    +'<h5> Telefono: ' + hh[i].phone + '<br>' + 'Fax: ' + hh[i].fax + '</h5>'
                    +'<h5> Email: ' + hh[i].email + '<br>' + 'Sito internet: <a href="' + hh[i].site + '">' + hh[i].site + '</a></h5>';

                description+='<h5>' + hh[i].description + '</h5>';
                address+='<h5>' + hh[i].address + '<br> Italia </h5>';
                services+='<p>' + hh[i].services + '</p>';
                    
            }

            
            $( "div.title" ).html(title);
            $( "div.immagine" ).html(pic);
            $( "div.informazioni" ).html(info);
            $( "div.descrizione" ).html(description);
            $( "div.indirizzo" ).html(address);
            $( "div.servizi" ).html(services);
        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });


    //GET HH PICTURES
    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getHH_pic.php", //Relative or absolute path to file.php file
        data: {id:id},
        success: function(response) {
            //console.log(JSON.parse(response));
            var images=JSON.parse(response);
            var gallery='';
            var caption='';
            var selector='';

                for(var i=0;i<images.length;i++){

                    if(images[i].num==0){
                        gallery+='<div class="active item" data-slide-number="' + images[i].num + '">'
                                +'<img src="/img/' + images[i].id + '">'
                                +'</div>';
                    }
                    else{
                        gallery+='<div class="item" data-slide-number="' + images[i].num + '">'
                                +'<img src="/img/' + images[i].id + '">'
                                +'</div>';
                    }
                                            
                    caption+='<div id="slide-content-' + images[i].num + '">'
                            +'<h2>' + images[i].caption + '</h2>'
                            +'</div>';

                    selector+='<li class="col-sm-2">'
                            +'<a class="thumbnail" id="carousel-selector-' + images[i].num + '"><img src="/img/' + images[i].id + '"></a>'
                            +'</li>';
            }

            $( "div.didascalia" ).html(caption);
            $( "div.galleria" ).html(gallery);
            $( "ul.patty" ).html(selector);
        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });


    //GET TRAILS RELATED
    
    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getTrails.php", //Relative or absolute path to file.php file
        data: {id:id},
        success: function(response) {
            //console.log(JSON.parse(response));
            var trails=JSON.parse(response);
            var el='';
            for(var i=0;i<trails.length;i++){
                //console.log(trails[i].id);
               
                if((i+1)%4==0) el+='<div class="row">';
                    
                el+='<div class="col-md-3 col-sm-6">'
                    +'<span class="thumbnail">'
                    +'<a href="/views/sentiero.html?id=' + trails[i].id + '">'
                    +'<img src="/img/' + trails[i].img + '" alt="...">'
                    +'<h4>' + trails[i].name + '</h4>'
                    +'<div class="ratings">';

                for(var j=0;j<trails[i].rate;j++){
                        el+='<span class="glyphicon glyphicon-star"></span>';
                }
                for(var j=0;j<(6-trails[i].rate);j++){
                        el+='<span class="glyphicon glyphicon-star-empty"></span>';
                }

                el+='</div>'
                    +'</a>'
                    +'<hr class="line">'
                    +'<p>Distanza: <b>'+ trails[i].distance  + ' km </b></p>'
                    +'<p>Durata: <b>'+ trails[i].last + ' ore </b></p>'
                    +'<p>Dislivello: <b>'+ trails[i].altitude + ' m </b></p>'
                    +'</span> </div>';

                if((i+1)%4==0) el+='</div>';

            }

            $( "div.trails" ).html(el);

        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });
}






function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));

}

