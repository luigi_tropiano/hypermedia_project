document.write('\
\
\
<script src="/js/jquery.js"></script>\
<script src="/js/menu.js"></script>\
\
<nav class="navbar navbar-inverse navbar-fixed-top">\
            <div class="container-fluid">\
                <!-- Brand and toggle get grouped for better mobile display -->\
                <div class="navbar-header">\
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">\
                        <span class="sr-only">Toggle navigation</span>\
                        <span class="icon-bar"></span>\
                        <span class="icon-bar"></span>\
                        <span class="icon-bar"></span>\
                    </button>\
                    <a class="navbar-brand navbar-brand-max" href="/index.html"><strong>Parco Naturale Adamello Brenta</strong></a>\
                    <a class="navbar-brand navbar-brand-min" href="/index.html"><strong>PNAB</strong></a>\
                </div>\
\
                <!-- Collect the nav links, forms, and other content for toggling -->\
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">\
                    <ul class="nav navbar-nav navbar-right">\
\
                        <li class="dropdown">\
                            <a href="/views/chi_siamo.html" class="chi_siamo dropdown-toggle" data-toggle="dropdown">Chi siamo </a>\
                            <ul class="dropdown-menu">\
                                <li><a href="/views/chi_siamo.html" class="chi_siamo-parco">Il parco</a></li>\
                                <li><a href="/views/chi_siamo-storia.html" class="chi_siamo-storia">Storia</a></li>\
                                <li><a href="/views/chi_siamo-organi_istituzionali.html" class="chi_siamo-organi_istituzionali">Organi Istituzionali</a></li>\
                                <li><a href="/views/chi_siamo-regole.html" class="chi_siamo-regole">Regole </a></li>\
                            </ul>\
                        </li>\
\
                        <li class="disabled"><a href="#">Dove siamo</a></li>\
\
                        <li class="dropdown">\
                            <a href="/views/sentieri.html" class="sentieri dropdown-toggle" data-toggle="dropdown">Sentieri </a>\
                            <ul class="dropdown-menu">\
                                <li><a href="/views/sentieri-by-diff.html?difficulty=easy" class="sentieri_facili">Facili</a></li>\
                                <li><a href="/views/sentieri-by-diff.html?difficulty=moderate" class="sentieri_moderati">Moderati</a></li>\
                                <li><a href="/views/sentieri-by-diff.html?difficulty=challenging" class="sentieri_impegnativi">Impegnativi</a></li>\
                                <li><a href="/views/sentieri-by-diff.html?difficulty=difficult" class="sentieri_difficili">Difficili</a></li>\
                                <li><a href="/views/sentieri-by-diff.html?difficulty=very_difficult" class="sentieri_molto_difficili">Molto difficili</a></li>\
                                <li><a href="/views/sentieri-by-diff.html?difficulty=extreme" class="sentieri_estremi">Estremi</a></li>\
                            </ul>\
                        </li>\
\
                        <li class="disabled"><a href="#">Natura e territorio</a></li>\
                        <li><a href="/views/rifugi.html" class="rifugi">Rifugi e ostelli</a></li>\
                        <li class="disabled"><a href="#">Attività educative</a></li>\
                    </ul>\
                </div><!-- /.navbar-collapse -->\
            </div><!-- /.container-fluid -->\
        </nav>\
');
        
