$(document).ready(ready);

function ready(){
    var id = getParameterByName('id');
    var diff = getParameterByName('diff');

    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getTrail.php", //Relative or absolute path to file.php file
        data: {id:id , diff:diff},
        success: function(response) {
            //console.log(JSON.parse(response));
            var trail=JSON.parse(response);
            var title='';
            var pic='';
            var info='';
            var description='';
            var starting='';
            var ending='';
            for(var i=0;i<trail.length;i++){
                //console.log(trail[i].id);

            title+='<h1>' + trail[i].name + '</h1>'
                +'<div class="ratings">';
            for(var j=0;j<trail[i].rate;j++){
                title+='<span class="glyphicon glyphicon-star"></span>';
            }
            for(var j=0;j<(6-trail[i].rate);j++){
                title+='<span class="glyphicon glyphicon-star-empty"></span>';
            }
            title+='</div> <br>'
                +'<div class="line"></div> <br>';

            pic+='<img src="/img/' + trail[i].img + '">';

            info+='<h4>' + trail[i].name + '<br></h4>'
                +'<h5>Distanza: <b>'+ trail[i].distance  + ' km </b></h5>'
                +'<h5>Durata: <b>'+ trail[i].last + ' ore </b></h5>'
                +'<h5>Dislivello: <b>'+ trail[i].altitude + ' m </b></h5>'
                +'<h5>Periodo consigliato: <b>'+ trail[i].period + ' </b></h5>'
                +'<h5>Attrezzatura: <b>'+ trail[i].equipment + ' </b></h5>'

            description+='<h4>' + trail[i].description + '</h4>';
            starting+=trail[i].beginning;
            ending+=trail[i].ending;
                
            //document.body.id = trail[0].difficulty;

            }

            
            $( "div.title" ).html(title);
            $( "div.immagine" ).html(pic);
            $( "div.informazioni" ).html(info);
            $( "div.descrizione" ).html(description);
            $( "div.partenza" ).html(starting);
            $( "div.arrivo" ).html(ending);

        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });

    //GET HHS PICTURES
    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getTrail_pic.php", //Relative or absolute path to file.php file
        data: {id:id},
        success: function(response) {
            //console.log(JSON.parse(response));
            var images=JSON.parse(response);
            var gallery='';
            var caption='';
            var selector='';

                for(var i=0;i<images.length;i++){

                    if(images[i].num==0){
                        gallery+='<div class="active item" data-slide-number="' + images[i].num + '">'
                                +'<img src="/img/' + images[i].id + '">'
                                +'</div>';
                    }
                    else{
                        gallery+='<div class="item" data-slide-number="' + images[i].num + '">'
                                +'<img src="/img/' + images[i].id + '">'
                                +'</div>';
                    }
                                            
                    caption+='<div id="slide-content-' + images[i].num + '">'
                            +'<h2>' + images[i].caption + '</h2>'
                            +'</div>';

                    selector+='<li class="col-sm-2">'
                            +'<a class="thumbnail" id="carousel-selector-' + images[i].num + '"><img src="/img/' + images[i].id + '"></a>'
                            +'</li>';
            }

            $( "div.didascalia" ).html(caption);
            $( "div.galleria" ).html(gallery);
            $( "ul.patty" ).html(selector);
        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });


    //GET RELATED HHS
    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getHHS.php", //Relative or absolute path to file.php file
        data: {id:id},
        success: function(response) {
            //console.log(JSON.parse(response));
            var hhs=JSON.parse(response);
            var el='';
            for(var i=0;i<hhs.length;i++){
                //console.log(trails[i].id);
               
                if((i+1)%4==0) el+='<div class="row">'; //insert a row each 4 elements

                el+='<div class="col-md-3 col-sm-6">'
                    +'<span class="thumbnail">'
                    +'<a href="/views/rifugio.html?id='+ hhs[i].id + '">'
                    +'<img src="/img/' + hhs[i].img + '" alt="...">'
                    +'<h4>' + hhs[i].name + '</h4>'
                    +'<div class="ratings">';

                        for(var j=0;j<hhs[i].rate;j++){
                            el+='<span class="glyphicon glyphicon-star"></span>';
                        }
                        for(var j=0;j<(5-hhs[i].rate);j++){
                            el+='<span class="glyphicon glyphicon-star-empty"></span>';
                        }

                el+='</div>'
                    +'<hr class="line">'
                    +'</a>'
                    +'<h5>' + hhs[i].address + '</h5>'
                    +'<h5>Telefono: ' + hhs[i].phone + '<br>' + 'Fax: ' + hhs[i].fax + '</h5>'
                    +'<div class="row">'
                    +'<h5>Email: ' + hhs[i].email + '<br>' + 'Sito internet:' + '<a href="' + hhs[i].site + '"> ' + hhs[i].site + '</a></h5>'
                    +'</div>'
                    +'</span>'
                    +'</div>';

                if((i+1)%4==0) el+='</div>';
            }

            $( "div.hhs" ).html(el);

        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });
}


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));

}

