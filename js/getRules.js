$(document).ready(ready);

function ready(){
    var type='RULES';
    
    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getAll.php", //Relative or absolute path to file.php file
        data: {type:type},
        success: function(response) {
            //console.log(JSON.parse(response));
            var rules=JSON.parse(response);
            var el='';
            for(var i=0;i<rules.length;i++){
                //console.log(rules[i].id);
                
                el+='<div class="media">'
                    + '<div class="media-left">'
                        + '<img class="media-object" src="/img/' + rules[i].img + '" alt="...">'
                    + '</div>'
                    + '<div class="media-body media-middle">'
                        + '<h4 class="media-heading">' + rules[i].description + ' </h4>'
                    + '</div>'
                    + '</div>';
                   
            }

            
            $( "div.rules" ).html(el);
        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });

}
