document.write('\
\
<footer role="contentinfo" class="site-footer" id="colophon">\
            <div class="container">\
                <div class="row">\
                    <div id="accordion" class="collapse-footer">\
                        <div class="panel">\
                            <div class="panel-heading">\
                                <h4 class="panel-title"> <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="collapsed"> <span class="glyphicon glyphicon-chevron-down"></span> </a> </h4>\
                            </div>\
                            <div class="panel-collapse collapse" id="collapseOne" style="height: 0px;">\
                                <div class="panel-body">\
                                    <div class="row">\
                                        \
                                        <div class="col-lg-3">\
                                            <h2><span>Parco Naturale Adamello Brenta</span></h2>\
                                        </div>\
                                        <div class="col-lg-3">\
                                            <h5 class="title"><span>Sede amministrativa</span></h5>\
                                            <p><br>Via Nazionale, 24 <br>38080 Strembo (TN) <br><br>Tel. 0465 806666 - Fax 0465 806699<br>P.IVA 01300650221 <br> C.F. 95006040224</p>\
                                        </div>\
                                        <div class="col-lg-3">\
                                            <h5 class="title"><span>Contattaci</span></h5>\
                                            <p><strong><br>Email:</strong> info@pnab.it - P.E.C. info@pec.pnab.it<br><br></p>\
                                            <a class="btn btn-default btn-sm" href="/views/form.html">Contattaci!</a>\
                                        </div>\
                                        \
                                        <div class="col-lg-3">\
                                            <h5 class="title"><span>Seguici</span></h5>\
                                            <br>\
                                            <a href="https://www.facebook.com/"><img src="/img/facebook-logo-button-c.png"0></a>\
                                            <a href="https://twitter.com/"><img src="/img/twitter-logo-button-c.png"></a>\
                                            <a href="https://it.linkedin.com/"><img src="/img/linkedin-button-c.png"></a>\
                                            <a href="https://plus.google.com/"><img src="/img/google-plus-logo-button-c.png"></a>\
                                        </div>\
\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
\
               <!-- Copyright -->\
            <div class="copyright">\
                <div class="container">\
                    <div class="row copyright-img">\
                        <div class="col-lg-8 col-sm-8 text-right" id="footertext"> Copyright © 2001-2016 Parco Naturale Adamello Brenta </div>\
                    </div>\
                </div>\
            </div>\
\
    </footer>\
\
 ');              
