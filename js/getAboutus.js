$(document).ready(ready);

function ready(){
    var type='ABOUTUS';
    
    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getAll.php", //Relative or absolute path to file.php file
        data: {type:type},
        success: function(response) {
            //console.log(JSON.parse(response));
            var aboutus=JSON.parse(response);
            var el='';
            for(var i=0;i<aboutus.length;i++){
                //console.log(aboutus[i].id);
                

                if(aboutus[i].img == ''){
                    el+='<h4>' + aboutus[i].title + '</h4>'
                        +'<p>' + aboutus[i].description + '</p>'; 
                    }
                else {
                    el+='<div class="col-md-8">'
                        +'<h4>' + aboutus[i].title + '</h4>'
                        +'<p>' + aboutus[i].description + '</p>' 
                        +'</div>'
                        +'<img class="col-md-4" src="/img/' + aboutus[i].img + '">';
                }
            }
            $( "div.activities" ).html(el);
        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });

    var type='PICTURES-ABOUTUS';
    
    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getAll.php", //Relative or absolute path to file.php file
        data: {type:type},
        success: function(response) {
            //console.log(JSON.parse(response));
            var pictures=JSON.parse(response);
            var el='';
            for(var i=0;i<pictures.length;i++){
                //console.log(pictures[i].id);
                    el+='<div class="col-md-3 col-sm-4 col-xs-12"><img class="img-responsive" src="/img/' + pictures[i].id + '" /></div>';
                }

            $( "div.pictures" ).html(el);
        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });
}
