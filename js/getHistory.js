$(document).ready(ready);

function ready(){
    var type='HISTORY';
    
    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getAll.php", //Relative or absolute path to file.php file
        data: {type:type},
        success: function(response) {
            //console.log(JSON.parse(response));
            var history=JSON.parse(response);
            var el1='';
            var el2='';
            var el3='';
            for(var i=0;i<history.length;i++){
                //console.log(rules[i].id);
                
                if(history[i].section == 'history') {
                    el1+='<h4>' + history[i].title + '</h4>'
                        + '<p>' + history[i].description + '</p> <br>';
                }

                if(history[i].section == 'adamello_brenta'){
                    el2+='<a id="geopark"><h3>' + history[i].title + '</h3></a><br>'
                        +'<img class="col-md-4" src="/img/' + history[i].img + '" alt="...">'
                        +'<div class="col-md-8">'
                        +'<p>' + history[i].description + '</p>' 
                        +'</div>';
                }

                if(history[i].section == 'dolomiti'){
                    el3+='<a id="patrimonio"><h3>' + history[i].title + '</h3></a><br>'
                        +'<img class="col-md-4" src="/img/' + history[i].img + '" alt="...">'
                        +'<div class="col-md-8">'
                        +'<p>' + history[i].description + '</p>'
                        +'</div>';
                }
                   
            }

            
            $( "div.history" ).html(el1);
            $( "div.geopark" ).html(el2);
            $( "div.dolomiti").html(el3);
        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });

}
