$(document).ready(ready);

function ready(){
    var type='HHS';
    
    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getAll.php", //Relative or absolute path to file.php file
        data: {type:type},
        success: function(response) {
            //console.log(JSON.parse(response));
            var hhs=JSON.parse(response);
            var el='';
            for(var i=0;i<hhs.length;i++){
                console.log(hhs[i].id);
                
                el+='<div class="col-md-3 col-sm-6">'
                    +'<span class="thumbnail">'
                    +'<a href="/views/rifugio.html?id='+ hhs[i].id + '">'
                    +'<img src="/img/' + hhs[i].img + '" alt="...">'
                    +'<h4>' + hhs[i].name + '</h4>'
                    +'<div class="ratings">';

                        for(var j=0;j<hhs[i].rate;j++){
                            el+='<span class="glyphicon glyphicon-star"></span>';
                        }
                        for(var j=0;j<(5-hhs[i].rate);j++){
                            el+='<span class="glyphicon glyphicon-star-empty"></span>';
                        }

                el+='</div>'
                    +'<hr class="line">'
                    +'</a>'
                    +'<h5>' + hhs[i].address + '</h5>'
                    +'<h5>Telefono: ' + hhs[i].phone + '<br>' + 'Fax: ' + hhs[i].fax + '</h5>'
                    +'<div class="row">'
                    +'<h5>Email: ' + hhs[i].email + '<br>' + 'Sito internet:' + '<a href="' + hhs[i].site + '"> ' + hhs[i].site + '</a></h5>'
                    +'</div>'
                    +'</span>'
                    +'</div>';
                   
            }

            
            $( "div.hhs" ).html(el);
        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });

}
