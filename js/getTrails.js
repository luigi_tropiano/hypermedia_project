$(document).ready(ready);

function ready(){
    var type=getParameterByName('difficulty');
    
    document.body.id = type;

    $.ajax({
        method: "GET",
        //dataType: "json", //type of data
        crossDomain: true, //localhost purposes
        url: "/php/getTrails.php", //Relative or absolute path to file.php file
        data: {type:type},
        success: function(response) {
            //console.log(JSON.parse(response));
            var trails=JSON.parse(response);
            var header='';
            var el='';
            for(var i=0;i<trails.length;i++){
                //console.log(trails[i].id);
               

                el+='<div class="col-md-3 col-sm-6">'
                    +'<span class="thumbnail">';
                if(type!=null)
                    el+='<a href="/views/sentiero.html?id=' + trails[i].id + '&diff=' + type + '">';
                else
                    el+='<a href="/views/sentiero.html?id=' + trails[i].id + '">';
                el+='<img src="/img/' + trails[i].img + '" alt="...">'
                    +'<h4>' + trails[i].name + '</h4>'
                    +'<div class="ratings">';

                for(var j=0;j<trails[i].rate;j++){
                        el+='<span class="glyphicon glyphicon-star"></span>';
                }
                for(var j=0;j<(6-trails[i].rate);j++){
                        el+='<span class="glyphicon glyphicon-star-empty"></span>';
                }

                el+='</div>'
                    +'</a>'
                    +'<hr class="line">'
                    +'<p>Distanza: <b>'+ trails[i].distance  + ' km </b></p>'
                    +'<p>Durata: <b>'+ trails[i].last + ' ore </b></p>'
                    +'<p>Dislivello: <b>'+ trails[i].altitude + ' m </b></p>'
                    +'</span> </div>';

            }

            header+='<h1>Sentieri ' + translationOf(type) + '</h1>'
                +'<div class="line"></div>';

            
            $( "div.trails" ).html(el);
            $( "div.page-header" ).html(header);

        },
        error: function(request,error) 
        {
            console.log("Error");
        }
    });
}

function translationOf(difficulty){
    if(difficulty=='easy') return 'facili';
    else if(difficulty=='moderate') return 'moderati';
    else if(difficulty=='challenging') return 'impegnativi';
    else if(difficulty=='difficult') return 'difficili';
    else if(difficulty=='very-difficult') return 'molto difficili';
    else return 'estremi';
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));

}
