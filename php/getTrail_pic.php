
<?php

require_once 'db_connect.php';

parse_parameters();

function parse_parameters() {
    if ($_GET['id'] != null) {
        $id = $_GET['id'];
        query_on_id($id);
    }
    else {
        $noparams = true;
    }
}

function query_on_id($id) {
    $mysqli = prepare_db_connection();

    $statement = $mysqli->prepare("SELECT * FROM `PICTURES-TRAILS` WHERE reference like ?");
    
    // Bind parameters
    $pid = 's'; // 's' is for String
    $param = "$id";
    $statement->bind_param($pid, $param);

    exec_query($statement);

    //close connection
    $mysqli->close();
}

?>
