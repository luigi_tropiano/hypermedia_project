<?php

require_once 'db_connect.php';

parse_parameters();

function parse_parameters() {
    if ($_GET['diff'] != null && $_GET['id'] != null) {
        $diff = $_GET['diff'];
        $id = $_GET['id'];
        query_on_both($id, $diff);
    }
    elseif ($_GET['id'] != null) {
        $id = $_GET['id'];
        query_on_id($id);
    }
    else {
        $noparams = true;
    }
}

function query_on_both($id, $diff) {
    $mysqli = prepare_db_connection();

    $statement = $mysqli->prepare("SELECT * FROM TRAILS WHERE id like ? AND difficulty like ?");
    
    // Bind parameters
    $pid = 'ss'; // 's' is for String
    $param1 = "%$id%";
    $param2 = "$diff";
    $statement->bind_param($pid, $param1, $param2);

    exec_query($statement);

    //close connection
    $mysqli->close();
}

function query_on_id($id) {
    $mysqli = prepare_db_connection();

    $statement = $mysqli->prepare("SELECT * FROM TRAILS WHERE id like ?");
    
    // Bind parameters
    $pid = 's'; // 's' is for String
    $param = "$id";
    $statement->bind_param($pid, $param);

    exec_query($statement);

    //close connection
    $mysqli->close();
}

?>
