<?php

require_once 'db_connect.php';

parse_parameters();

function parse_parameters() {
    if ($_GET['type'] != null) {
        $type = $_GET['type'];
        query_on_type($type);
    }
    else {
        $noparams = true;
        echo "No parameter passed".mysqli_connect_error(); //notify error
        exit(); //do nothing else 
    }
}

function query_on_type($type) {
    $mysqli = prepare_db_connection();

    if  ($type == 'ABOUTUS')
        $statement = $mysqli->prepare("SELECT * FROM ABOUTUS");
    elseif($type == 'HISTORY')
        $statement = $mysqli->prepare("SELECT * FROM HISTORY");
    elseif($type == 'RULES')
        $statement = $mysqli->prepare("SELECT * FROM RULES");
    elseif($type == 'PICTURES-ABOUTUS')
        $statement = $mysqli->prepare("SELECT * FROM `PICTURES-ABOUTUS`");
    elseif($type == 'TRAILS')
        $statement = $mysqli->prepare("SELECT * FROM TRAILS");
    elseif($type == 'HHS')
        $statement = $mysqli->prepare("SELECT id,name,address,phone,fax,email,site,rate,img FROM HH");
    
    exec_query($statement);

    //close connection
    $mysqli->close();
}


?>
