<?php

require_once 'db_connect.php';

parse_parameters();

function parse_parameters() {
    if ($_GET['type'] != null) {
        $type = $_GET['type'];
        query_on_type($type);
    }
    elseif ($_GET['id'] != null) {
        $id = $_GET['id'];
        query_by_hh($id);
    }
    else {
        $noparams = true;
    }
}

function query_on_type($type) {
    $mysqli = prepare_db_connection();

    $statement = $mysqli->prepare("SELECT * FROM TRAILS WHERE difficulty=?");
    
    // Bind parameters
    $ptype = 's'; // 's' is for String
    $param = "$type";
    $statement->bind_param($ptype, $param);

    exec_query($statement);

    //close connection
    $mysqli->close();
}

function query_by_hh($id) {
    $mysqli = prepare_db_connection();

    $statement = $mysqli->prepare("SELECT * FROM `TRAILS`
                                    WHERE id IN (SELECT trail FROM `TRAILS-HH`
                                    WHERE hh like ? )");
        
    // Bind parameters
    $pid = 's'; // 's' is for String
    $param = "%$id%";
    $statement->bind_param($pid, $param);

    exec_query($statement);

    //close connection
    $mysqli->close();
}

?>
