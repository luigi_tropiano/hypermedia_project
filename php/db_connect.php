<?php

require_once 'config.php';

function prepare_db_connection() {
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    mysqli_set_charset($mysqli,'utf8');

    /* Verify connection was successful */
    if (mysqli_connect_errno()) {
        echo "Error to connect to DBMS: ".mysqli_connect_error();
        exit();
    }

    return $mysqli;
}

function exec_query($statement) {
    // Execute the statement
    $statement->execute();

    // Get the result 
    $result = $statement->get_result();

    $myArray = array();//create an array
    while ($row = $result->fetch_assoc()) {
        $myArray[] = $row;
    }

    // return the fetched values
    echo json_encode($myArray);

    //close statement
    $statement->free_result();
}

?>
